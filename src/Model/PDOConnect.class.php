<?php

namespace App\Model;

/**
 * Fichier de connexion à la base de donnée
 */
class PDOConnect {
	/**
	 * PDO object
	 *
	 * @var \PDO
	 */
	private $DB;

	
	public function __construct($config) {
		$host = $config["Database"]["host"];
		$dbname = $config["Database"]["dbname"];
		$user = $config["Database"]["user"];
		$pass = $config["Database"]["pass"];
		
		$this->DB = new \PDO('pgsql:host='.$host.';dbname='.$dbname,$user, $pass);
	}


	/**
	 * Getter PDO object
	 *
	 * @return \PDO
	 */
	public function getDB() { return $this->DB; }
}
