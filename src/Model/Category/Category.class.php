<?php

namespace App\Model\Category;

class ManagerCategoryModel {
    private $db_PDO;

    public function __construct(\PDO $db) { $this->setDbPDO($db); }

    public function setDbPDO(\PDO $db) { $this->db_PDO = $db; }

    public function getDbPDO(): \PDO{ return $this->db_PDO; }

    public function getAllCategory() {
        $selectAll = ($this->db_PDO)->query('SELECT * FROM "Category"');

        return $selectAll;
    }

    public function newCategory(string $name) {
        $newCategory = ($this->db_PDO)->prepare('INSERT INTO "Category"(name) VALUES(?)');
        $newCategory->execute(array($name));

        return $newCategory->rowCount();
    } 
}