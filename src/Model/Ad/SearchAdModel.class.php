<?php

namespace App\Model\Ad;

class SearchAdModel {
    private $db_PDO;

    public function __construct(\PDO $db) { 
        $this->setDbPDO($db);
    }

    public function setDbPDO(\PDO $db) { $this->db_PDO = $db; }

    public function getDbPDO(): \PDO{ return $this->db_PDO; }

    public function searchAd(string $word, string $category) {
        $search = ($this->db_PDO)->prepare('SELECT * FROM "Ad" 
        WHERE ("Ad".name ~* :word OR description ~* :word) AND (id_category = :category OR :category = -1) 
        ORDER BY date_stop DESC');
        $search->execute((array(
            "word" => ".*$word.*",
            "category" => $category
        )));

        return $search;
    }

    public function searchAdPerPage(string $word, string $category, int $min, int $max, string $sort, int $id_user = -1, string $pseudo_diff) {
        $search = ($this->db_PDO)->prepare('SELECT ad.*, u.pseudo FROM "Ad" ad 
        INNER JOIN "User" u
        ON u.id = ad.id_user 
        WHERE (ad.name ~* :word OR description ~* :word) AND (id_category = :category OR :category = -1 AND (id_user = :id_user OR :id_user = -1)) 
        AND u.pseudo <> :pseudo
        ' . $sort . ' 
        LIMIT :min OFFSET :max');
        $search->execute((array(
            "pseudo" => $pseudo_diff,
            "word" => ".*$word.*",
            "category" => $category,
            "min" => $max - $min,
            "max" => $min,
            "id_user" => $id_user
        )));

        return $search;
    }

    public function getAd(int $id) {
        $search = ($this->db_PDO)->prepare('SELECT ad.*, u.pseudo, u.mail FROM "Ad" ad 
        INNER JOIN "User" u
        ON u.id = ad.id_user
        WHERE ad.id = ?');
        $search->execute(array($id));
        
        return $search->fetch();
    }

    public function countAd(int $id_user = -1, int $id_user_diff = -1) {
        $count = ($this->db_PDO)->prepare('SELECT COUNT(id) as "totalAd" FROM "Ad" WHERE (? = -1 OR id_user = ?) AND (? = -1 OR id_user <> ?)');
        $count->execute(array($id_user, $id_user, $id_user_diff, $id_user_diff));
        
        return $count->fetch();
    }

    public function countAdSearch(string $word, string $category) {
        $count = ($this->db_PDO)->prepare('SELECT COUNT(id) as "totalAd" FROM "Ad" 
        WHERE ("Ad".name ~* :word OR description ~* :word) AND (id_category = :category OR :category = -1) ');
        $count->execute((array(
            "word" => ".*$word.*",
            "category" => $category
        )));

        return $count->fetch();
    }
}

