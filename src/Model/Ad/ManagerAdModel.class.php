<?php

namespace App\Model\Ad;

use App\Module\Ad\Ad;
use App\Module\Category\Category;

class ManagerAdModel {
    private $db_PDO;

    public function __construct(\PDO $db) { $this->setDbPDO($db); }

    public function setDbPDO(\PDO $db) { $this->db_PDO = $db; }

    public function getDbPDO(): \PDO{ return $this->db_PDO; }

    public function newAd(Ad $ad) {
        $new_ad = ($this->db_PDO)->prepare('INSERT INTO "Ad"(id_user, name, id_category, description, price_unit_start, current_price, picture, album, date_stop, date_adding)
        VALUES(:id_user, :name, :id_category, :description, :price_unit_start, :current_price, :picture, :album, :date_stop, NOW())');

    $new_ad->execute(array(
            ":id_user" => $ad->getIdUser(),
            ":name" => $ad->getName(),
            ":id_category" => ($ad->getCategory() != null) ? ($ad->getCategory())->getId() : null,
            ":description" => $ad->getDescription(),
            ":price_unit_start" => $ad->getPriceStart(),
            ":current_price" => $ad->getPriceStart(),
            ":picture" => $ad->getPicture(),
            ":album" => $ad->getAlbum(),
            ":date_stop" => $ad->getDateStop()
        ));

        return $new_ad->rowCount();
    }

    public function issetPseudo(string $pseudo) {
        $pseudo_isset = ($this->db_PDO)->prepare('SELECT id FROM "User" WHERE pseudo = ?');
        
        $pseudo_isset->execute((
            $pseudo
        ));

        return $pseudo_isset;
    }

    public function issetMain(string $mail) {
        $mail_isset = ($this->db_PDO)->prepare('SELECT id FROM "User" WHERE mail = ?');
        
        $mail_isset->execute((
            $mail
        ));

        return $mail_isset;
    }

    public function closeAd(int $id_user, int $îd_ad) {
        $update = ($this->db_PDO)->prepare('UPDATE "Ad" SET status_ad = \'close\' WHERE id = :id AND id_user = :id_user');

        $update->execute(array(
            "id" => $îd_ad,
            "id_user" => $id_user
        ));

        return $update->rowCount();
    }

    public function delAd(int $id_user, int $id_ad) {
        $delete = ($this->db_PDO)->prepare('DELETE FROM "Ad" WHERE id = :id AND id_user = :id_user');

        $delete->execute(array(
            "id" => $id_ad,
            "id_user" => $id_user
        ));

        return $delete->rowCount();
    }
}
