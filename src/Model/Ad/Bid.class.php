<?php

namespace App\Model\Bid;

class BidModel {
    private $db_PDO;

    public function __construct(\PDO & $db) { $this->setDbPDO($db); }

    public function setDbPDO(\PDO $db) { $this->db_PDO = $db; }

    public function getDbPDO(): \PDO{ return $this->db_PDO; }

    public function updateCurrentPrice(int $id_ad, float $new_price) {
        $update = ($this->db_PDO)->prepare('UPDATE "Ad" SET current_price = :price WHERE current_price < :price AND id = :id_ad AND date_stop > NOW()');
        $update->execute(array(
            "price" => $new_price,
            "id_ad" => $id_ad
        ));

        return $update->rowCount();
    }

    public function addNewBid(int $id_ad, int $id_buyer, int $price) {
        $add = ($this->db_PDO)->prepare('INSERT INTO "Bid"(id_ad, id_buyer, price)
        VALUES(:id_ad, :id_buyer, :price)');

        $add->execute(array(
            "id_ad" => $id_ad,
            "id_buyer" => $id_buyer,
            "price" => $price
        ));

        return $add->rowCount();
    }

    public function getAllBid(int $id_ad) {
        $select = ($this->db_PDO)->prepare('SELECT pseudo, price, date_bid FROM "Bid" as b
        INNER JOIN "User" as u
        ON u.id = b.id_buyer
         WHERE id_ad = ?
         ORDER BY date_bid DESC');
        $select->execute(array($id_ad));

        return $select->fetchAll();
    }

    public function listBid(string $id, int $min, int $max) {
        $search = ($this->db_PDO)->prepare('SELECT ad.id, name, price, current_price, pseudo, description, picture, status_ad, date_stop  FROM "Bid" b

        INNER JOIN "Ad" ad
        ON ad.id = b.id_ad

        INNER JOIN "User" u
        ON u.id = ad.id_user

        WHERE price IN (SELECT MAX(price)  FROM "Bid"  WHERE id_buyer = :id
        GROUP BY id_ad)
        ORDER BY b.date_bid DESC

        -- LIMIT :min OFFSET :max'
        );
        $search->execute((array(
            "id" => $id
            // "min" => $max - $min,
            // "max" => $min
        )));

        return $search;
    }

    public function countBid(int $id_user) {
        $count = ($this->db_PDO)->prepare('SELECT COUNT(id) as "totalBid" FROM "Bid" WHERE id_buyer = ? GROUP BY id_ad');
        $count->execute(array($id_user));

        $c = $count->fetch();
        
        return !$c ? ["totalBid" => 0] : $c;
    }
}
