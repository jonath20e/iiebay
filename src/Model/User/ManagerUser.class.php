<?php

namespace App\Model\User;

use App\Module\User\User;

class ManagerUser {
    private $db_PDO;

    public function __construct(\PDO $db) { 
        $this->setDbPDO($db);
    }

    public function setDbPDO(\PDO $db) { $this->db_PDO = $db; }

    public function getDbPDO(): \PDO{ return $this->db_PDO; }

    public function getAllUser(int $min, int $max, string $sort) {
        $select = ($this->db_PDO)->prepare('SELECT * FROM "User"
        ORDER BY id DESC
        LIMIT :min OFFSET :max
        ');
        $select->execute((array(
            "min" => $max - $min,
            "max" => $min
        )));

        return $select->fetchAll();
    }

    public function getCountUser() {
        $count = ($this->db_PDO)->query('SELECT count(id) as totalUser FROM "User"');

        return $count->fetch();
    }

    public function ban(int $id) {
        $ban = ($this->db_PDO)->prepare('UPDATE "User" SET status = \'ban\' WHERE id = ?');
        $ban->execute(array($id));

        return $ban->rowCount();
    }

    public function unban(int $id) {
        $ban = ($this->db_PDO)->prepare('UPDATE "User" SET status = \'user\' WHERE id = ?');
        $ban->execute(array($id));

        return $ban->rowCount();
    }

    public function addUser(User $user) {
        $insert = ($this->db_PDO)->prepare('INSERT INTO "User"(lastname, firstname, pseudo, mail, password, status, date_subscribe) 
        VALUES(:lastname, :firstname, :pseudo, :mail, :password, :status, NOW())');

        $insert->execute((array(
            "lastname" => $user->getLastName(),
            "firstname" => $user->getFirstName(),
            "pseudo" => $user->getPseudo(),
            "mail" => $user->getMail(),
            "password" => $user->getPassWord(),
            "status" => $user->getStatus()
        )));

        return $insert->rowCOunt();
    }

    public function issetPseudo(string $pseudo) {
        $select = ($this->db_PDO)->prepare('SELECT id FROM "User" WHERE pseudo = ?');

        $select->execute(array($pseudo));

        return $select->fetch();
    }

    public function issetMail(string $mail) {
        $select = ($this->db_PDO)->prepare('SELECT id FROM "User" WHERE mail = ?');

        $select->execute(array($mail));

        return $select->fetch();
    }

    public function changeProfil(User $user) {
        $insert = ($this->db_PDO)->prepare('UPDATE "User" SET lastname = :lastname, firstname = :firstname, pseudo = :pseudo, mail = :mail, password = :password WHERE id = :id');

        $insert->execute((array(
            "id" => $user->getId(),
            "lastname" => $user->getLastName(),
            "firstname" => $user->getFirstName(),
            "pseudo" => $user->getPseudo(),
            "mail" => $user->getMail(),
            "password" => $user->getPassWord()
        )));

        return $insert->rowCOunt();
    }
}