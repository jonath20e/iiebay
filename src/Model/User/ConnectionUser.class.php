<?php

namespace App\Model\User;

use App\Module\User\User;

class ConnectionUser {
    private $db_PDO;

    public function __construct(\PDO $db) { 
        $this->setDbPDO($db);
    }

    public function setDbPDO(\PDO $db) { $this->db_PDO = $db; }

    public function getDbPDO(): \PDO{ return $this->db_PDO; }

    public function getUser(string $mail, string $passsword) {
        $select = ($this->db_PDO)->prepare('SELECT * FROM "User" WHERE mail = ? AND password = ?');

        $select->execute(array(
            $mail,
            $passsword
        ));

        return $select->fetch();
    }
}
