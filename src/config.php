<?php
/**
 * Configuration des routes
 * Configuration de la base de donnée
 */

use App\Module\File\PictureFile;

return [
    "Web" => [
        "url" => "http://localhost/pima/src/"
    ],
    "Database" => [
        "host"=> "localhost",
        "dbname" => "iiebay",
        "user"=> "postgres",
        "pass"=> "azerty"
    ],
    "ConfigRoute" => [
        "Route404" => [
            "name" => "404",
            "path" => "Controller/404.php"
        ],
        "Routes"=> [
            [
                "name" => "",
                "path" => "Controller/IndexController.class.php"
            ],
            [
                "name" => "accueil",
                "path" => "Controller/IndexController.class.php"
            ],
            [
                "name" => "post-annonce",
                "path" => "Controller/EnchereController.class.php"
            ],
            [
                "name" => "search",
                "path" => "Controller/SearchController.class.php"
            ],
            [
                "name" => "subscribe",
                "path" => "Controller/SubscribeController.class.php"
            ],
            [
                "name" => "auth",
                "path" => "Controller/AuthController.class.php"
            ],
            [
                "name" => "disconnect",
                "path" => "Controller/DisconnectController.class.php"
            ],
            [
                "name" => "view-enchere",
                "path" => "Controller/ViewEnchereController.class.php"
            ],
            [
                "name" => "profil",
                "path" => "Controller/ProfilController.class.php"
            ],
            [
                "name" => "rating",
                "path" => "Controller/RatingController.class.php"
            ],
            [
                "name" => "sales",
                "path" => "Controller/SalesController.class.php"
            ],
            [
                "name" => "bid",
                "path" => "Controller/BidController.class.php"
            ],
            [
                "name" => "admin-user",
                "path" => "Controller/AdminUserController.class.php"
            ]
        ]
    ],
    "Ad" => [
        "UploadImage" => [
            "path" => "Public/pictures/",
            "size" => [
                "constraint" => "App\Module\Constraint\ConstraintBasic\ConstraintLessEqual",
                "param" => 5 * 10000000 // En octets
            ],
            "extension" => [
                "constraint" => "App\Module\Constraint\ConstraintRange\ConstraintRange",
                "param" => PictureFile::$EXTENSIONS
            ]
        ], 
        /**
         * Configuration des contraintes du formulaire
         */
        "Fields" => [
            "name" => [
                "min" => 2,
                "max" => 50
            ],
            "description" => [
                "min" => 10,
                "max" => 1000
            ],
            "date_stop" => time() + 30, // time = date actuelle en timestamp en secondes,
            "price_start" => 0.100000
        ]
    ],
    "Search" => [
        "Pagination" => [
            "result_per_page" => 5
        ]
    ]
];