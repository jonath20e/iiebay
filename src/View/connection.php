<!DOCTYPE html>
<html lang="fr">
  <head>
		<meta charset="utf-8">
		<link rel="stylesheet" href="<?php echo $this->CONFIG["Web"]["url"]; ?>Public/css/subscribe.css">
		<?php require_once("require_link.php"); ?>
    <title> Creez dès maintenant votre enchère ! </title>
  </head>
<body>
	<?php require_once("nav.php"); ?>
		<div class="container">
			<div class="row main"> 
				<div class="main-login main-center">
					<form class="form-horizontal" method="post" action="">
						
						<div class="form-group">
							<label for="email" class="cols-sm-2 control-label">Votre email</label>
							<div class="cols-sm-10">
								<div class="input-group">
									<span class="input-group-addon"><i class="glyphicon glyphicon-user" aria-hidden="true"></i></span>
									<input type="mail" class="form-control" name="mail" id="mail" autocomplete="off"  placeholder="Entrer votre email"/>
								</div>
							</div>
						</div>



						<div class="form-group">
							<label for="password" class="cols-sm-2 control-label">Votre mot de passe</label>
							<div class="cols-sm-10">
								<div class="input-group">
									<span class="input-group-addon"><i class="glyphicon glyphicon-lock" aria-hidden="true"></i></span>
										<input type="password" class="form-control" name="password" id="password"  placeholder="Entrer le mot de passe"/>
								</div>
							</div>
						</div>

						<div class="form-group ">
							<button type="butsubmitton" class="btn btn-primary btn-lg btn-block login-button">Se connecter </button>
						</div>
						<?php echo $error; ?>
						
					</form>
				</div>
			</div>
		</div>

		<!-- <script type="text/javascript" src="assets/js/bootstrap.js"></script> -->
	</body>
</html>
