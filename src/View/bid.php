<!DOCTYPE html>
<html lang="fr">
  <head>
		<meta charset="utf-8">
		<?php require_once("require_link.php"); ?>
    <title> Creez dès maintenant votre enchère ! </title>
  </head>
<body>
	<?php require_once("nav.php"); ?>
		<div class="container">
		<style>
			.list-group-item {
	min-height: 180px;
}
.list-group-item img {
	width: 12%;
	float: left;
}
.list-group-item:hover {
	background-color: #f2f2f2;
}
/* .close { color: red; } */
.open { color: green; }
.date_hidden { display: none; }
	</style>
<body>
	<?php require_once("nav.php"); ?>
		<div class="container">
			<ul class="list-group">
			<?php foreach ($result_all as $bid) : ?>
  <li class="list-group-item">
	<strong><?php echo htmlspecialchars($bid["name"]); ?></strong> <span class="status <?php echo ($bid["status_ad"] == 'open' ? 'open' : ''); ?>">[<?php echo $bid["status_ad"]; ?>]</span>&nbsp;&nbsp;<span class="date"></span><span class="date_hidden"><?php echo $bid["status_ad"] != 'close' ? $bid["date_stop"] : 'close'; ?></span>
	<br />
	<img src="<?php echo $this->CONFIG["Web"]["url"] . ($bid["picture"] != '' && !is_null($bid["picture"]) ? $bid["picture"] : 'Public/unvailable.png'); ?>" />
	<strong>Description : </strong>
	<?php echo htmlspecialchars($bid["description"]); ?>
	<br />
	<br />
	<a href="<?php echo $this->CONFIG["Web"]["url"]; ?>view-enchere/enchere/<?php echo $bid["id"]; ?>" class="btn btn-default" role="button">Historique</a>
	</li>
	</li>
	<?php endforeach; ?>
</ul>
<div style="text-align: center;">
<?php echo $pagination->generateTemplate($template) ?>
</div>
		</div>
	</body>
	<script src="<?php echo $this->CONFIG["Web"]["url"]; ?>Public/js/jquery.js"></script>
	<script>
		$(function() {
      function resetDate() {
      var date_left_hidden = $(".date_hidden");
      var date_left = $(".date");
			var status = $('.status');
			var close_link = $('.close_link');
			var del_link = $('.del_link');
      var content = '';
      var inter;
      var inter_date;

      for (var i = 0; i < date_left_hidden.length; i++) {
        content = $(date_left_hidden[i]).html();
        inter = (new Date(content)).getTime() - Date.now();
        inter_date = new Date(inter);
        
        if (inter_date == 'Invalid Date' || inter <= 0 || $(status[i]).text() == '[close]') {
          if (content == 'close') {
            $(status[i]).html("Enchère close par l'auteur");
						$(date_left[i]).html('');
					} else {
            $(status[i]).html("Enchère terminée");
						$(date_left[i]).html('');
					}
						$(close_link[i]).addClass('disabled');
						$(del_link[i]).addClass('disabled');

          	$(status[i]).css('color', 'red');
        }else {
          $(date_left[i]).html(parseInt((parseInt(inter / 1000)) / (3600 * 24)) + " jours " + inter_date.getHours() + " heures " +inter_date.getMinutes() + " minutes " + inter_date.getSeconds() + " secondes restantes");
          if (inter < (1000 * 3600 * 6))
            $(date_left[i]).css('color', 'orange');
          else
            $(date_left[i]).css('color', 'green');
        }
      }
      }
      resetDate();
      setInterval(resetDate, 1000);

		});
	</script>
		</div>
	</body>
</html>
    