<!DOCTYPE html>
<html lang="fr">
	<head>
		<title>Inscription</title>
		<meta charset="UTF-8" />
		<link rel="stylesheet" href="<?php echo $this->CONFIG["Web"]["url"]; ?>Public/css/subscribe.css">
		<?php require_once("require_link.php"); ?>
	</head>
	<style>

	/* body, html{
     height: 100%;
 	background-repeat: no-repeat;
 	background-color: #d3d3d3;
 	font-family: 'Oxygen', sans-serif;
} */


</style>
<body>
<?php require_once("nav.php") ?>
		<div class="container">
			<div class="row main"> 
				<div class="main-login main-center">
					<form class="form-horizontal" method="post" action="">
						
						<div class="form-group">
							<label for="lastname" class="cols-sm-2 control-label">Votre nom</label>
							<div class="cols-sm-10">
								<div class="input-group">
									<span class="input-group-addon"><i class="fa fa-user fa" aria-hidden="true"></i></span>
									<input type="text" class="form-control" name="lastname" id="lastname"  placeholder="Entrer votre nom" value="<?php if (isset($_POST['lastname'])) {echo $_POST['lastname']; } ?>"/>
								</div>
							</div>
                            <label for="firstname" class="cols-sm-2 control-label">Votre prénom</label>
                            <div class="cols-sm-10">
								<div class="input-group">
									<span class="input-group-addon"><i class="fa fa-user fa" aria-hidden="true"></i></span>
									<input type="text" class="form-control" name="firstname" id="firstname"  placeholder="Entrer votre prénom" value="<?php if (isset($_POST['firstname'])) {echo $_POST['firstname']; } ?>"/>
								</div>
							</div>
						</div>
						
                        <div class="form-group">
							<label for="pseudo" class="cols-sm-2 control-label">Votre pseudo</label>
							<div class="cols-sm-10">
								<div class="input-group">
									<span class="input-group-addon"><i class="fa fa-users fa" aria-hidden="true"></i></span>
									<input type="text" class="form-control" name="pseudo" id="pseudo"  placeholder="Entrer your pseudonyme" value="<?php if (isset($_POST['pseudo'])) {echo $_POST['pseudo']; } ?>"/>
								</div>
							</div>
						</div>
                        
						<div class="form-group">
							<label for="email" class="cols-sm-2 control-label">Votre email</label>
							<div class="cols-sm-10">
								<div class="input-group">
									<span class="input-group-addon"><i class="fa fa-envelope fa" aria-hidden="true"></i></span>
									<input type="mail" class="form-control" name="mail" id="mail"  placeholder="Entrer votre email" value="<?php if (isset($_POST['mail'])) {echo $_POST['mail']; } ?>"/>
								</div>
							</div>
						</div>



						<div class="form-group">
							<label for="password" class="cols-sm-2 control-label">Mot de passe</label>
							<div class="cols-sm-10">
								<div class="input-group">
									<span class="input-group-addon"><i class="fa fa-lock fa-lg" aria-hidden="true"></i></span>
									<input type="password" class="form-control" name="password" id="password"  placeholder="Entrer le mot de passe"/>
								</div>
							</div>
						</div>

						<div class="form-group">
							<label for="confirmm_password" class="cols-sm-2 control-label">Confirmation de mot de passe</label>
							<div class="cols-sm-10">
								<div class="input-group">
									<span class="input-group-addon"><i class="fa fa-lock fa-lg" aria-hidden="true"></i></span>
									<input type="password" class="form-control" name="confirmm_password" id="confirmm_password"  placeholder="Confirmation de mot de passe"/>
								</div>
							</div>
						</div>

						<div class="form-group ">
							<button type="butsubmitton" class="btn btn-primary btn-lg btn-block login-button">Enregistrer </button>
						</div>
						<div class="login-register">
				            <a href="index.php">Se connecter</a>
						 </div>
						 <?php echo $success; ?>
					</form>
				</div>
			</div>
		</div>

		<script type="text/javascript" src="assets/js/bootstrap.js"></script>
	</body>
</html>