<!DOCTYPE html>
<html lang="fr">

  <head>
		<meta charset="utf-8">
		<meta charset="UTF-8" />
		<?php require_once("require_link.php"); ?>
    <title> Créez dès maintenant votre enchère !</title>
  </head>
  <body>
	<?php require_once("nav.php") ?>
	<?php if (isset($_SESSION["user"])) { ?>
    <div style="text-align: center;">
			<h2 style="font-style: italic;">Proposez dès maintenant vos objets aux iiens !</h2>
			<hr />
     	<br/>
			<br/>
	    
			 <form action="post-annonce" method="post" enctype="multipart/form-data">
			 <!-- style="margin-left: 15px" -->
				<label for="name_ad" ><strong>Nom de l'objet à vendre</strong></label>
				<br/>
				<input type="text" name="name_ad" id="name_ad" autocomplete="off" required />
				<br/>
				<label for="description" ><strong>Description</strong></label>
				<br/>
				<input type="text" name="description" id="description" autocomplete="off" />
				<br/>
				<br/>
				<label for="date_de_fin" style="margin-left: 15px"><strong>Fin de l'enchère</strong></label>
				<br/>
				<input type="datetime-local" id="date_de_fin" name="date_de_fin" required>
				<br/>
				 <!-- placeholder="AAAA-MM-JJ" -->
				<!-- <label for="lieu_event" style="margin-left: 15px"><strong>Lieu de retrait</strong></label>
				<br/>
				<input type="text" name="lieu" style="width=100%;" required> -->
				<br/>
				<label for="price" style="margin-left: 15px"><strong>Prix (euros)</strong></label>
				<br/>
				<input type="number" min="0.10" step="any" name="price" id="price" required>
				<br />
				  <label for="categories"><strong>Catégorie</strong>  </label>
					<br />
      <select  name="category">
        <option value="-1" selected>Catégories</option>
        <?php foreach ($categories as $category): ?>
        <option value="<?php echo $category->getId(); ?>"><?php echo $category->getName(); ?></option>
        <?php EndForeach; ?>
      </select>
	  <br />
      <label for="pic" style="margin-left:15px"><strong>Photo principale de l'objet</strong></label>
      <br />
      <input style="text-align:center !important;" type="file" id="pic" name="pic" />
      <br />
      <label for="pics" style="margin-left:15px"><strong> D'autres images (2 maximum)</strong></label>
      <br />
      <input type="file" name="pics[]" multiple />
      <br />
			<input type="hidden" name="form" />
	  <input type="submit" value="Soumettre l'annonce" />
	     </form>
			 <?php echo $success; ?>
	<?php }else {  ?>
		<h2 style="text-align:center;color:red;">Veuillez vous connecter pour déposer une annonce</h2>
	<?php } ?>
    </div>
  </body>
</html>

  
      
