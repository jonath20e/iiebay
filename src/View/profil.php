<!DOCTYPE html>
<html lang="fr">
	<head>
		<title>Changer les donnée de votre profile</title>
		<meta charset="UTF-8" />
		<link rel="stylesheet" href="<?php echo $this->CONFIG["Web"]["url"]; ?>Public/css/subscribe.css">
		<?php require_once("require_link.php"); ?>
	</head>
	<style>

	/* body, html{
     height: 100%;
 	background-repeat: no-repeat;
 	background-color: #d3d3d3;
 	font-family: 'Oxygen', sans-serif;
} */


</style>
<body>
<?php require_once("nav.php") ?>
		<div class="container">
			<div class="row main"> 
				<div class="main-login main-center">
					<form class="form-horizontal" method="post" action="">
						
						<div class="form-group">
						<h4 id="title_password">Changer les données personnelles</h4>
							<label for="lastname" class="cols-sm-2 control-label">Votre nouveau nom</label>
							<div class="cols-sm-10">
								<div class="input-group">
									<span class="input-group-addon"><i class="fa fa-user fa" aria-hidden="true"></i></span>
									<input type="text" class="form-control" name="lastname" id="lastname"  placeholder="Entrer votre nouveau nom" value="<?php echo $current_user->getLastname(); ?>"/>
								</div>
							</div>
                            <label for="firstname" class="cols-sm-2 control-label">Votre nouveau prénom</label>
                            <div class="cols-sm-10">
								<div class="input-group">
									<span class="input-group-addon"><i class="fa fa-user fa" aria-hidden="true"></i></span>
									<input type="text" class="form-control" name="firstname" id="firstname"  placeholder="Entrer votre nouveau prénom" value="<?php echo $current_user->getFirstName(); ?>"/>
								</div>
							</div>
						</div>
						
                        <div class="form-group">
							<label for="pseudo" class="cols-sm-2 control-label">Votre nouveau pseudo</label>
							<div class="cols-sm-10">
								<div class="input-group">
									<span class="input-group-addon"><i class="fa fa-users fa" aria-hidden="true"></i></span>
									<input type="text" class="form-control" name="pseudo" id="pseudo"  placeholder="Entrer votre nouveau pseudo" value="<?php echo $current_user->getPseudo(); ?>"/>
								</div>
							</div>
						</div>
                        
						<div class="form-group">
							<label for="email" class="cols-sm-2 control-label">Votre nouvel email</label>
							<div class="cols-sm-10">
								<div class="input-group">
									<span class="input-group-addon"><i class="fa fa-envelope fa" aria-hidden="true"></i></span>
									<input type="mail" class="form-control" name="mail" id="mail"  placeholder="Entrer votre nouvel email" value="<?php echo $current_user->getMail(); ?>"/>
								</div>
							</div>
						</div>


						<div class="form-group">
						<h4 id="title_password">Changer de mot de passe</h4>
							<label for="old_password" class="cols-sm-2 control-label">Réécrivez votre mot de passe d'origine</label>
							<div class="cols-sm-10">
								<div class="input-group">
									<span class="input-group-addon"><i class="fa fa-lock fa-lg" aria-hidden="true"></i></span>
									<input type="password" class="form-control" name="old_password" id="old_password"  placeholder="Entrer le nouveau  mot de passe"/>
								</div>
							</div>
						</div>

						<div class="form-group">
							<label for="password" class="cols-sm-2 control-label">Votre nouveau mot de passe</label>
							<div class="cols-sm-10">
								<div class="input-group">
									<span class="input-group-addon"><i class="fa fa-lock fa-lg" aria-hidden="true"></i></span>
									<input type="password" class="form-control" name="password" id="password"  placeholder="Confirmation du nouveau mot de passe"/>
								</div>
							</div>
						</div>
						<div class="form-group">
							<label for="confirmm_password" class="cols-sm-2 control-label">Confirmation du nouveau mot de passe</label>
							<div class="cols-sm-10">
								<div class="input-group">
									<span class="input-group-addon"><i class="fa fa-lock fa-lg" aria-hidden="true"></i></span>
									<input type="password" class="form-control" name="confirmm_password" id="confirmm_password"  placeholder="Confirmation du nouveau mot de passe"/>
								</div>
							</div>
						</div>

						<div class="form-group ">
							<input type="submit" class="btn btn-primary btn-lg btn-block login-button" value="Enregistrer" />
						</div>
						<?php echo $success; ?>
					</form>
				</div>
			</div>
		</div>

		<script type="text/javascript" src="assets/js/bootstrap.js"></script>
	</body>
</html>
