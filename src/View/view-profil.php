<!DOCTYPE html>
<html lang="fr">
      <head>
		<meta charset="utf-8" />
	<title> Consulter le profil de *requete php* </title>
	<style>
l.notes-echelle {
	margin:0;
	padding:0;
	font:.75em/1.2 Arial, Helvetica, sans-serif;
}
ul.notes-echelle li {
	float:left;
	margin:0;
	padding:0;
	list-style:none;
	min-width:20px;
	min-height:20px;
}
/* Correctif IE6 sur min-width & min-height */
* html ul.notes-echelle.js li {
	width:20px;
	height:20px;
}
ul.notes-echelle li label {
	display:block;
	text-align:center;
	line-height:20px;
	background:url(etoiles.gif) center top no-repeat;
	cursor:pointer;
}
</style>
</head>
	<body>
	<ul class="notes-echelle">
	<li>
		<label for="note01" title="Note&nbsp;: 1 sur 3">1</label>
		<input type="radio" name="notesA" id="note01" value="1" />
	</li>
	<li>
		<label for="note02" title="Note&nbsp;: 2 sur 3">2</label>
		<input type="radio" name="notesA" id="note02" value="2" />
	</li>
	<li>
		<label for="note03" title="Note&nbsp;: 3 sur 3">3</label>
		<input type="radio" name="notesA" id="note03" value="3" />
	</li>
	<li>
		<label for="note04" title="Note&nbsp;: 4 sur 5">4</label>
		<input type="radio" name="notesA" id="note04" value="4" />
	</li>
    <li>
		<label for="note05" title="Note&nbsp;: 5 sur 5">5</label>
		<input type="radio" name="notesA" id="note05" value="5" />
	</li>
</ul>
<br/>
<br/>
<input type="button" name="Submit" id="Submit" value="Submit">
</body>

</html>
