<!DOCTYPE html>
<html lang="fr">
  <head>
		<meta charset="utf-8">
		<?php require_once("require_link.php"); ?>
    <title> Creez dès maintenant votre enchère ! </title>
  </head>
	<style>
.table {
	background-color: white;
}
.table th {
	text-align: center;
}
.table td {
	padding-top: 2% !important;
	padding-bottom: 2% !important;
	text-align: center;
}
	</style>
<body>
	<?php require_once("nav.php"); ?>
		<div class="container">
			
<table class="table table-hover table-bordered">
    <thead>
      <tr>
        <th>Pseudo</th>
        <th>Nom</th>
        <th>Prénom</th>
        <th>Email</th>
				<th>Statut</th>
				<th>Gérer</th>
      </tr>
    </thead>
    <tbody>
		<?php foreach($users as $user): ?>
		<?php if($user["status"] != 'admin') { ?>
      <tr>
        <td><?php echo $user["pseudo"]; ?></td>
        <td><?php echo $user["lastname"]; ?></td>
        <td><?php echo $user["firstname"]; ?></td>
        <td><?php echo $user["mail"]; ?></td>
        <td><?php echo ($user["status"] == 'ban') ? 'Utilisateur banni' : 'OK'; ?></td>
        <td>
				<a class="btn btn-info" href="<?php echo $this->CONFIG["Web"]["url"] . 'admin-user/info/' . $user["id"]; ?>">Regarder les ventes utilisateurs</a>
				<a class="btn btn-danger <?php echo ($user["status"] == 'ban') ? 'disabled' : ''; ?>" href="<?php echo $this->CONFIG["Web"]["url"] . 'admin-user/ban/' . $user["id"]; ?>">Bannir</a>
				<a class="btn btn-primary <?php echo ($user["status"] == 'user') ? 'disabled' : ''; ?>" href="<?php echo $this->CONFIG["Web"]["url"] . 'admin-user/unban/' . $user["id"]; ?>">Débannir</a>
				</td>
      </tr>
		<?php } ?>
<?php endforeach; ?>
    </tbody>
  </table>
	<br />
  <br />
<?php echo $pagination->generateTemplate($template) ?>
		</div>
	</body>
</html>
    