<!DOCTYPE html>
<html lang="fr">
  <head>
		<meta charset="utf-8">
		<?php require_once("require_link.php"); ?>
    <title> Creez dès maintenant votre enchère ! </title>
	</head>
	<style>
#panel_ad img {
	width: 20%;
	float: left;
	margin-right: 2%;
}
#album img {
	width: 20%;
}
#album {
	margin-left: 2%;
}
#state_ad {
	margin-left: 2%;
}
#log {
	margin-left: 2%;
}
#text_current_price { color: #681E0E; }
.date_hidden { display: none;}
#log {
	height: 200px;
	    overflow: scroll;

}
	</style>
<body>
	<?php require_once("nav.php"); ?>
		<div class="container">
			<div class="panel panel-default">
			<div class="panel-body">
				<div id="panel_ad">
						<img src="<?php echo $this->CONFIG["Web"]["url"] . (is_null($ad["ad"]->getPicture()) || $ad["ad"]->getPicture() == '' ? "Public/unvailable.png" : $ad["ad"]->getPicture()); ?>" alt="ui" />
						<h3><strong><?php echo htmlspecialchars($ad["ad"]->getName()); ?></strong></h3>
						<h5>Auteur du post : <strong><?php echo htmlspecialchars($ad["pseudo"]); ?></strong></h5>
						<br />
						<strong>Description : </strong>
<?php echo htmlspecialchars($ad["ad"]->getDescription()); ?>
			</div>

			<!-- <div id="album">
				<h3>Album</h3>
				<img src="<?php echo $this->CONFIG["Web"]["url"];?>Public/unvailable.png" alt="ui" />
				<img src="<?php echo $this->CONFIG["Web"]["url"];?>Public/unvailable.png" alt="ui" />
			</div> -->
			<hr />
			<div id="state_ad">
						<span id="text_current_price">Prix actuel : <span id="current_price"><?php echo $ad["ad"]->getCurrentPrice(); ?></span> euros</span>
						<br />
						<br />
						<?php if (isset($_SESSION["user"])) { ?>
						<div class="form-group">
						    <h6 class="card-subtitle mb-2 text-muted date"><?php echo htmlspecialchars($ad["ad"]->getDateStop()); ?></h6>
      						<h6 class="card-subtitle mb-2 text-muted date_hidden"><?php echo htmlspecialchars($ad["ad"]->getDateStop()); ?></h6>
							<br />
							<br />
							<br />
							<br />
							<label for="price" class="cols-sm-2 control-label">Votre prix (euros)</label>
							<div class="cols-sm-10">
								<div class="input-group">
									<input type="price" class="form-control" name="price" id="price" autocomplete="off" placeholder="Entrer votre prix"/>
									<br />
									<br />
									<button type="button" id="button_price" class="btn btn-primary">Enchèrir</button>
								</div>
							</div>
						</div>
						<?php } ?>
			</div>
<br />
							<br />
							<br /><br />
							<br />
							<br />
							<h4>Contact : <em><?php echo $ad["mail"]; ?></h4>
			<div class="panel panel-default">
				<div class="panel-heading">Historique des utilisateurs</div>
 				<div class="panel-body" id="log">
					 <?php foreach ($offers as $offer): ?>
								<strong><?php echo $offer["pseudo"] ?></strong> : <?php echo $offer["price"] . " euros"; ?> le <em><?php echo $offer["date_bid"]; ?></em> <br /><hr />
					 <?php Endforeach; ?>
				</div>

			</div>
			
		</div>
		</div>
	</body>
		<script src="<?php echo $this->CONFIG["Web"]["url"]; ?>Public/js/jquery.js"></script>
	<script>
		$(function() {
      function resetDate() {
              var date_left_hidden = $(".date_hidden");
      var date_left = $(".date");
      var content = '';
      var inter;
      var inter_date;

      for (var i = 0; i < date_left_hidden.length; i++) {
        content = $(date_left_hidden[i]).html();
        inter = (new Date(content)).getTime() - Date.now();
        inter_date = new Date(inter);
        
        if (inter_date == 'Invalid Date' || inter <= 0) {
            $(date_left[i]).html("Enchère terminée");
            $(date_left[i]).css('color', 'red');
        }else {
          $(date_left[i]).html(parseInt((parseInt(inter / 1000)) / (3600 * 24)) + " jours " + inter_date.getHours() + " heures " +inter_date.getMinutes() + " minutes " + inter_date.getSeconds() + " secondes restants");
          if (inter < (1000 * 3600 * 6))
            $(date_left[i]).css('color', 'orange');
          else
            $(date_left[i]).css('color', 'green');
        }
      }
      }
      resetDate();
      setInterval(resetDate, 1000);

		
		<?php if (isset($_SESSION["user"])) { ?>
		var ws = new WebSocket('ws://localhost:8080');

		var token = { 
			"mail" : "<?php echo $_SESSION["user"]->getMail(); ?>",
			"token" : "<?php echo $_SESSION["user"]->getPassword(); ?>",
			"code" : {
				"type" : "sendEnchere",
				"id" :  <?php echo (int) $this->PARAM["parameters"]["enchere"]; ?>,
				"value" : 14
			}
		};

		console.log("Affichage Token -> ", token);
		 ws.onerror = function () {
            alert("Le serveur des enchères est actuellement indisponible");
        }

        ws.onopen = function () {
                console.log('Connexion établie');
				$('#button_price').click(function() {
					var check = ($('#price').val() == '') || confirm("Voulez-vous enchèrir avec une somme de " + $('#price').val() + " ?");
					if (check) {
						token.code.value = $('#price').val();
						ws.send(JSON.stringify(token));
					}
				});
                
               
                ws.onmessage = function (evt) {
                    if (typeof (evt.data) === "string") {
                        console.log("Message reçu: " + evt.data);
						var data = JSON.parse(evt.data);
						if (typeof data.error !== 'undefined' && data.error != '') {
							alert("Vous devez indiquer un montant supérieur au montant actuel");
							$('#price').val("");
						}
						else if (typeof data.success !== 'undefined' && data.success != '')
							alert("Transation effectuée");
						else if (typeof data.price !== 'undefined' && typeof data.histo !== 'undefined') {
							$('#current_price').text(data.price);
							console.log(data.histo);
							$('#log').prepend( "<strong>" + data.histo.pseudo + "</strong> : " + data.histo.price + " euros le <em>" + (new Date(Date.now()).toISOString()) + " </em> <br /><hr />" );
							 $('#price').val("");
						}
						// if (evtdata == '')
                    }
                };
               
        }
		});
	<?php }  ?>
	</script>
</html>
    