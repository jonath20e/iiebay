<nav class="navbar navbar-inverse">
  <div class="container-fluid">
    <div class="navbar-header">
      <a class="navbar-brand" href="<?php echo $this->CONFIG["Web"]["url"]; ?>">IIEbay</a>
    </div>
    <ul class="nav navbar-nav">
      <li class="active"><a href="<?php echo $this->CONFIG["Web"]["url"]; ?>accueil">Accueil</a></li>
      <li><a href="<?php echo $this->CONFIG["Web"]["url"]; ?>post-annonce">Déposer une annonce</a></li>
      <li><a href="<?php echo $this->CONFIG["Web"]["url"]; ?>search">Rechercher une annonce</a></li>
      <?php if (isset($_SESSION["user"])) { ?>
      <li><a href="<?php echo $this->CONFIG["Web"]["url"]; ?>profil">Modifier mon profil</a></li>
      <!-- <li><a href="<?php echo $this->CONFIG["Web"]["url"]; ?>rating">Notes des utilisateurs</a></li> -->
      <li><a href="<?php echo $this->CONFIG["Web"]["url"]; ?>sales">Mes ventes</a></li>
      <li><a href="<?php echo $this->CONFIG["Web"]["url"]; ?>bid">Mes offres</a></li>
      <?php } ?>
    </ul>
    <?php if (!isset($_SESSION["user"])) { ?>
    <ul class="nav navbar-nav navbar-right">
      <li class=><a href="<?php echo $this->CONFIG["Web"]["url"]; ?>subscribe"><span class="glyphicon glyphicon-user"></span> Créer un compte</a></li>
      <li><a href="<?php echo $this->CONFIG["Web"]["url"]; ?>auth"><span class="glyphicon glyphicon-log-in"></span> Se connecter</a></li>
    </ul>
    <?php } ?>
    <?php if (isset($_SESSION["user"])) { ?>

    <ul class="nav navbar-nav navbar-right">
            <?php if (($_SESSION["user"])->getStatus() == 'admin') { ?>
        <li><a href="<?php echo $this->CONFIG["Web"]["url"]; ?>admin-user"><span class="glyphicon glyphicon-user"></span> Gestion des utilisateurs</a></li>
      <?php 
    } ?>
      <li><a href="<?php echo $this->CONFIG["Web"]["url"]; ?>disconnect"><span class="glyphicon glyphicon-off"></span> Déconnexion</a></li>
    </ul>
    <?php } ?>
  </div>
</nav>