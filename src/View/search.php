<!DOCTYPE html>
<html lang="fr">
  <head>
		<meta charset="utf-8" />
		<meta charset="UTF-8" />
		<?php require_once("require_link.php"); ?>
    <title> Rechercher des annonces</title>
  </head>
<html>
<style>
#list_result * {
  background-color: #d3d3d3 !important;
}
.card-group {
  display: flex;
  flex-flow: row wrap;
  justify-content: center;
  padding: 5px;
  overflow: hidden;
  position: relative;
}
.card {
    box-shadow: 0 4px 8px 0 rgba(0,0,0,0.2);
    transition: 0.3s;
    width: 14%;
    background-color: white;
    padding: 10px;
    margin-top: 10px;
    margin-right: 10px;
    height: 470px;
}
/* .card-link {
  position: absolute;
  bottom:20px;
  margin-left: -20px;
} */
.card:hover {
    box-shadow: 0 8px 16px 0 rgba(0,0,0,0.2);
    background-color: #F7F9F9;
}
.card-img {
  width: 100%;
  height: 250px;
}
.date_hidden { display: none;}
@media screen and (max-width: 600px) {
    .card {font-size: 8px;width: 40%;height: 300px;}
    .card-group {  }
    .card-img {width: 70%;height: 125px;}
}
@media screen and (max-width: 1000px) {
    .card {font-size: 12px;width: 40%;height: 300px;}
    .card-group {  }
    .card-img {width: 40%;height: 125px;}
}
</style>
<body>
<?php require_once("nav.php") ?>
<form post="search" method="post">

<div style="text-align: center;">
  		<h2 style="font-style: italic;">Rechercher des annonces !</h2>
			<hr />
     	<br/>
			<br/>
  <div>
    <label for="name_ad"><strong>Titre de l'annonce</strong></label>
    <br />
      <input type="text" name="name_ad" id="name_ad" autocomplete="off" value="<?php echo (isset($_SESSION["save_search"]) ? $_SESSION["save_search"]["name_ad"] : "") ?>" placeholder="Que recherchez-vous ? ">
    
    <br />
    <div>
      <label for="category"><strong>Catégories</strong></label>
      <br />
      <select  name="category" id="category">
        <option value="-1" selected>Catégories</option>
        <?php foreach ($categories as $category): ?>
        <option <?php echo isset($_SESSION["save_search"]) && $_SESSION["save_search"]["category"] == $category->getId() ? 'selected="selected"' : ''; ?> value="<?php echo $category->getId(); ?>"><?php echo $category->getName(); ?></option>
        <?php EndForeach; ?>
      </select>
      <br />
       <label for="sort"><strong>Trier par : </strong></label>
      <br />
      <select  name="sort" id="sort">
        <option value="-1" selected>Choisisez le type de tri</option>
        <?php foreach ($sorted as $key => $sort): ?>
        <option <?php echo isset($_SESSION["save_search"]) && $_SESSION["save_search"]["sort"] == $key ? 'selected="selected"' : ''; ?> value="<?php echo $key; ?>"><?php echo $sort["text"]; ?></option>
        <?php EndForeach; ?>
      </select>
    </div>


  <button type="submit">Rechercher</button>

  </form>

  <div class="card-group">
<?php foreach ($ads as $ad): ?>
  <div class="card">
    <div class="card-body">
      <img class="card-img" src="<?php echo ($ad["ad"]->getPicture() != null) ? $this->CONFIG["Web"]["url"] . $ad["ad"]->getPicture() : $this->CONFIG["Web"]["url"] . "Public/unvailable.png"; ?>" />
      <h5 class="card-title"><?php echo htmlspecialchars($ad["ad"]->getName()); ?>&nbsp;</h5>
      <h5 class="card-title">Auteur du post : <strong><?php echo htmlspecialchars($ad["pseudo"]); ?></strong>&nbsp;</h5>

      <h6 class="card-subtitle mb-2">Prix actuel : <strong><?php echo htmlspecialchars($ad["ad"]->getCurrentPrice()); ?> euros</strong></h6>
      <h6 class="card-subtitle mb-2 text-muted date"><?php echo htmlspecialchars($ad["ad"]->getDateStop()); ?></h6>
      <h6 class="card-subtitle mb-2 text-muted date_hidden"><?php echo ($ad["ad"]->getStatus() == 'open' ? htmlspecialchars($ad["ad"]->getDateStop()) : htmlspecialchars($ad["ad"]->getStatus())); ?></h6>
      <p class="card-text"><?php echo ($ad["ad"]->getDescription() == null) ? "<em>Aucune description</em>" : substr(htmlspecialchars($ad["ad"]->getDescription()), 0, 100) . "..."; ?></p>
<?php if ((strtotime($ad["ad"]->getDateStop()) - time()) > 0) {  ?><a href="<?php echo $this->CONFIG["Web"]["url"] . "view-enchere/enchere/" . $ad["ad"]->getId(); ?> " class="card-link">Enchérir</a> <?php } ?>
    </div>
  </div>
  
  <?php EndForeach; ?>
</div>
  <br />
  <br />
<?php echo $pagination->generateTemplate($template) ?>
</div>
</body>
	<script src="<?php echo $this->CONFIG["Web"]["url"]; ?>Public/js/jquery.js"></script>
	<script>
		$(function() {
      function resetDate() {
      var date_left_hidden = $(".date_hidden");
      var date_left = $(".date");
      var content = '';
      var inter;
      var inter_date;

      for (var i = 0; i < date_left_hidden.length; i++) {
        content = $(date_left_hidden[i]).html();
        inter = (new Date(content)).getTime() - Date.now();
        inter_date = new Date(inter);
        
        if (inter_date == 'Invalid Date' || inter <= 0) {
          if (content == 'close')
            $(date_left[i]).html("Enchère close par l'auteur");
          else
            $(date_left[i]).html("Enchère terminée");
          
          $(date_left[i]).css('color', 'red');
        }else {
          $(date_left[i]).html(parseInt((parseInt(inter / 1000)) / (3600 * 24)) + " jours " + inter_date.getHours() + " heures " +inter_date.getMinutes() + " minutes " + inter_date.getSeconds() + " secondes restants");
          if (inter < (1000 * 3600 * 6))
            $(date_left[i]).css('color', 'orange');
          else
            $(date_left[i]).css('color', 'green');
        }
      }
      }
      resetDate();
      setInterval(resetDate, 1000);

		});
	</script>
</html>