<?php

namespace App;

require_once('../vendor/autoload.php');

use App\Module\Route\Route;
use App\Module\Route\Router;

use App\Module\Session\Session;

use App\Model\PDOConnect;

/**
 * Classe qui traite le lancement de l'application web
 */
class App {
  /**
   * Une méthode qui met en route l'application web
   *
   * @return void
   */
  public static function run() {
    SESSION::start();
    $config = require_once('config.php');
        
    try {  
      $DB_PDO = new PDOConnect($config);
    }catch (\Exception $e) {
      var_dump($e->getMessage());
			die('<p>Erreur: database connection has failed</p>');
		}
    $DB = $DB_PDO->getDB();

    /**
     * Chargement des routes
     */
    $router = self::runRoute($config);

    $route_target = $router->getInfoRoute($_GET['mode']);

    $PARAM = Router::getParameters($_GET['mode']);
    
    /**
     * Chargement de la page cible
     */
    require_once($route_target['path']);
  }


  /**
   * Chargement des routes
   *
   * @param array $config
   * @return Router
   */
  public static function runRoute($config): Router {
    $_GET['mode'] = $_GET['mode'] ?? '';

    /**
     * On instancie la variable qui permettra de "rediriger" les requêtes http
     * @var Router
     */
    $router = new Router();

    $router->setRoute404(new Route($config["ConfigRoute"]["Route404"]["name"], $config["ConfigRoute"]["Route404"]["path"]));

    foreach($config["ConfigRoute"]["Routes"] as $route)
      $router->add(new Route($route["name"], $route["path"]));

    $router->run();

    return $router;
  }

  public static function render(string $namespace, string $file, \PDO & $DB, ?array & $CONFIG = null, ?array & $PARAM = null) {
    $render = $namespace . '\\' . explode('.', basename($file))[0];
    (new $render($DB, $CONFIG, $PARAM))->render();
  }
}

/**
 * Lancement de l'application
 */
App::run();


?>
