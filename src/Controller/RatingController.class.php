<?php

namespace App\Controller;

use \App\App;

class RatingController extends Controller {
    private $DB;
    private $CONFIG;
    
    public function __construct(\PDO & $DB, ?array & $CONFIG, ?array & $PARAM) {
        $this->DB = $DB;
        $this->CONFIG = $CONFIG;
        parent::__construct("view-profil.php");
    }

    public function render() {     
             
        require_once("View/" . $this->template);
    }
}

App::render(__NAMESPACE__, __FILE__, $DB, $config);

?>