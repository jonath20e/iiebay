<?php

namespace App\Controller;

use App\Module\Constraint\ConstraintBasic\ConstraintLessEqual;
use App\Module\Constraint\ConstraintMultiple;

use App\Module\Constraint\ConstraintRange\ConstraintRange;
use App\Module\Constraint\ConstraintRange\ConstraintBetween;
use App\Module\Constraint\ConstraintFormat\ConstraintPattern;

use \App\App;
use App\Module\User\ManagerUser;
use App\Module\User\User;

class ProfilController extends Controller {
    private $DB;
    private $CONFIG;
    
    public function __construct(\PDO & $DB, ?array & $CONFIG, ?array & $PARAM) {
        $this->DB = $DB;
        $this->CONFIG = $CONFIG;
        parent::__construct("profil.php");
    }

    public function render() {
        $current_user = ManagerUser::loadCurrentUser();

        $success = '';
        if (isset($_SESSION["user"]) && isset($_POST["lastname"]) && isset($_POST["firstname"]) && isset($_POST["pseudo"]) && isset($_POST["old_password"]) && isset($_POST["password"]) && isset($_POST["confirmm_password"])) {
                 $success = '';
        $manager_user = new ManagerUser($this->DB, new ConstraintMultiple([
            "lastname" =>  new ConstraintBetween([
                    "min" => 2,
                    "max" => 50
                ]),
            "firstname" => new ConstraintBetween([
                    "min" => 2,
                    "max" => 50
                ]),
            "pseudo" => new ConstraintBetween([
                    "min" => 2,
                    "max" => 50
                ]),
            "mail" => new ConstraintPattern("#^[a-zA-Z0-9._-]{1,255}@[a-zA-Z0-9._-]{2,25}\.[a-z]{2,4}$#"),
            "password" => new ConstraintBetween([
                    "min" => 6,
                    "max" => 50
                ]),
            "status" => new ConstraintRange(ManagerUser::$status)
        ]));

                $exec = true;
            if (trim(ManagerUser::hashPassword($_POST["old_password"], "JDkn6d@.")) != trim($_SESSION["user"]->getPassword())) {
                $success = '<span style="color:red ;">Mot de passe d\'origine incorrecte</span>';
                $exec = false;
            }
            if ($exec && $_POST["password"] != $_POST["confirmm_password"]) {
                $success = '<span style="color:red ;">La confirmation du mot de passe est incorrecte</span>';
                $exec = false;
            }

            $user = new User($_SESSION["user"]->getId(), $_POST["lastname"], $_POST["firstname"], $_POST["pseudo"], $_POST["mail"], $_POST["password"], $_SESSION["user"]->getStatus());
            $result;
            if ($exec && ($result = $manager_user->updateUser($current_user, $user)) == ManagerUser::$SUCCESS) {
                    $_SESSION["user"]->setLastName($_POST["lastname"]);
                    $_SESSION["user"]->setFirstName($_POST["firstname"]);
                    $_SESSION["user"]->setPseudo($_POST["pseudo"]);
                    $_SESSION["user"]->setMail($_POST["mail"]);
                    $_SESSION["user"]->setPassword(ManagerUser::hashPassword($_POST["password"], "JDkn6d@."));
                    
                    $success = '<span style="color: green";>Modification effectuée</span>';
            } else {
                     $matches = [
            ManagerUser::$LAST_NAME_ERROR => "nom",
            ManagerUser::$FIRST_NAME_ERROR => "prénom",
            ManagerUser::$PSEUDO_ERROR => "pseudo",
            ManagerUser::$PASSWORD_ERROR => "mot de passe"
        ];  

            if (array_key_exists($exec ? $result : 45, $matches))
                $success = '<span style="color: red;">Le ' . $matches[$result] . ' doit être compris entre 2 et 50 caractères !</span>';
   
            if ($exec && $result == ManagerUser::$PSEUDO_ISSET)
                $success = '<span style="color: red;">Le pseudo existe déjà !</span>';

            if ($exec && $result == ManagerUser::$MAIL_ISSET)
                $success = '<span style="color: red;">Le mail existe déjà !</span>';
            
            if ($exec && $result == ManagerUser::$SUCCESS) {
                $success = '<span style="color: green;">Le compte a été créé avec succès !</span>';
                $_POST = [];
            }


            }

        }
             
        require_once("View/" . $this->template);
    }
}

App::render(__NAMESPACE__, __FILE__, $DB, $config);

?>