<?php

namespace App\Controller;

use \App\App;
use App\Module\Category\Traits\TraitManagerCategory;

use App\Module\Category\Category;

use App\Module\Ad\Ad;
use App\Module\Ad\ManagerAd;

use App\Module\File\File;
use App\Module\File\PictureFile;
use App\Module\Upload\ManagerUpload;

use App\Module\Constraint\ConstraintBasic\ConstraintGreaterEqual;
use App\Module\Constraint\ConstraintRange\ConstraintBetween;
use App\Module\Constraint\ConstraintMultiple;

use App\Module\User\ManagerUser;

class EnchereController extends Controller {
    use TraitManagerCategory;
    private $DB;
    private $CONFIG;
    
    private $success;

    public function __construct(\PDO & $DB, ?array & $CONFIG, ?array & $PARAM) {
        $this->DB = $DB;
        $this->CONFIG = $CONFIG;
        parent::__construct("enchere.php");
        $this->setSuccess('');
    }

    public function getSuccess(): string { return $this->success;}

    public function setSuccess(string $success) { $this->success = $success; }

    public function deleteDir($path_save, $dir_ad) {
        if (file_exists($path_save . $dir_ad)) {
            $dels = scandir($path_save . $dir_ad); 
            foreach ($dels as $del) { 
                if ($del != "." && $del != "..")
                    unlink($path_save . $dir_ad . '/' . $del); 
                // echo $del;
            }
            rmdir($path_save . $dir_ad);
        }
    }

    public function checkNewAd(): bool {
        if (isset($_POST['form'])) {
            if (isset($_POST['name_ad']) && isset($_POST['price']) && isset($_POST['description']) && isset($_POST['date_de_fin']) && isset($_POST['category'])) {
                /**
                 * Upload de fichiers : /Public/pictures/id_user/nomAdIncrementeSiExiste
                 */
                $user = ManagerUser::loadCurrentUser();
                // var_dump($user);
                if (is_null($user))
                    return false;
                $id_user = $user->getId();
                $path_save = $this->CONFIG["Ad"]["UploadImage"]["path"] . $id_user . '/';
                if (!file_exists($path_save)) {
                    if (!mkdir($path_save, 0777)) {
                        $this->setSuccess('<span style="color: red;">Un problème est survenu lors de l\'upload (code 1)</span>');
                        return false;   
                    }
                }


                /**
                 * On enregistre un nom de dossier pour l'annonce qui contiendra les images liées à l'annonce
                 */
                $dir_ad = sha1(mt_rand());

                while (file_exists($path_save . $dir_ad))
                    $dir_ad = sha1(mt_rand());

                $manager_upload = new ManagerUpload($path_save . $dir_ad . '/');

                // var_dump($this->CONFIG["UploadImage"]);
                
                /**
                 * Contraintes sur les images
                 */
                $manager_upload->setConstraint(
                    new ConstraintMultiple([
                        "size" => new $this->CONFIG["Ad"]["UploadImage"]["size"]["constraint"]($this->CONFIG["Ad"]["UploadImage"]["size"]["param"]),
                        "extension" => new $this->CONFIG["Ad"]["UploadImage"]["extension"]["constraint"]($this->CONFIG["Ad"]["UploadImage"]["extension"]["param"])
                    ])
                );

                /**
                 * Chargement de l'image principal
                 */
                $file = File::loadFromForm('pic');

                    if (is_null($file) || ($file != [] && count($file) != 1)) {
                        $this->setSuccess('<span style="color: red;">Un problème est survenu lors de l\'upload (code 2)</span>');
                        return false;
                    }
                    // var_dump($file);
                
                $album = "";
                /**
                 * Chargement de l'alnum
                 */
                $files = File::loadFromForm('pics');
                //  var_dump($_FILES);
                //     var_dump(is_null($files));

                    if (is_null($files) || ($files != [] && count($files) > 2)) {
                        $this->setSuccess('<span style="color: red;">Un problème est survenu lors de l\'upload de l\'album(code 2)</span>');
                        return false;
                    }
            

                /**
                 * Si l'image principal n'est pas reçu, alors on ne crée pas d'album
                 */
                // var_dump($file[0]->getPath());
                if (!is_null($file[0]->getPath())) {
                    if (!mkdir($path_save . $dir_ad, 0777)) {
                            $this->setSuccess('<span style="color: red;">Un problème est survenu lors de l\'upload (code 3)</span>');
                            return false;   
                    }

                    $pic_array = array_merge([$file[0]], ($files != [] ? $files : []));
                                    // var_dump($pic_array);
                    

                    foreach ($pic_array as $key => $pic) {
                        if ($key == 0)
                            $pic->setName('pictureAd');
                        else {
                            $pic->setName('album' . $key);
                            $album .= trim($path_save . $dir_ad . '/' . $pic->getName() . '.' . $pic->getExtension()) . ':';
                        }
        
                        /**
                         * Upload
                         */
                        $result = $manager_upload->upload($pic);
        
                        /**
                         * En cas d'échec
                         */
                        if ($result == ManagerUpload::$FILE_ERROR) {
                            $this->deleteDir($path_save, $dir_ad);
                            $this->setSuccess('<span style="color: red;">Un problème est survenu lors de l\'upload</span>');
                            return false;
                        }
                        
                        if ($result == ManagerUpload::$EXTENSION_ERROR) {
                            $this->deleteDir($path_save, $dir_ad);
                            $this->setSuccess('<span style="color: red;">Le ou les fichier(s) envoyé(s) ne correspond(ent) pas à une ou des image(s)</span>');
                            return false;
                        }
        
                        if ($result == ManagerUpload::$SIZE_ERROR) {
                            $this->deleteDir($path_save, $dir_ad);
                            $this->setSuccess('<span style="color: red;">Le fichier a dépassé la limite de taille autorisée</span>');
                            return false;
                        }
                    }
                }

         
            
                /**
                 * Enregistrement d'une annonce
                 */
                $manager_ad = new ManagerAd($this->DB);

                /**
                 * Contraintes sur les variables du formulaire reçue
                 */
                $manager_ad->setConstraint(
                    new ConstraintMultiple([
                        "name" => new ConstraintBetween($this->CONFIG["Ad"]["Fields"]["name"]),
                        "description" => new ConstraintBetween($this->CONFIG["Ad"]["Fields"]["description"]),
                        "date_stop" => new ConstraintGreaterEqual($this->CONFIG["Ad"]["Fields"]["date_stop"]),
                        "price_start" => new ConstraintGreaterEqual($this->CONFIG["Ad"]["Fields"]["price_start"])
                    ])
                );
                // var_dump($_POST);

                $success = $manager_ad->newAd(new Ad(   
                    $_POST['name_ad'],
                    (float) $_POST['price'],
                    $_POST['date_de_fin'],
                    ($_POST['description'] != '') ? (($_POST['description'])) : null,
                    !is_null($file[0]->getPath()) ? trim($path_save . $dir_ad . '/' . $file[0]->getName() . '.' . $file[0]->getExtension()) : null,
                    $album,
                    new Category("", ((int) $_POST['category'] > 0) ? (int) $_POST['category'] : null),
                    null, 
                    $id_user
                ));
                // var_dump($success);
                switch ($success) {
                    case ManagerAd::$SUCCESS:
                        $this->setSuccess('<span style="color: green;">Annonce ajoutée</span>');
                        return true;
                    break;

                    case ManagerAd::$NAME_ERROR:
                        $this->setSuccess('<span style="color: red;">Le titre d\'une annonce doit être compris entre 2 et 50 caractères </span>');
                        $this->deleteDir($path_save, $dir_ad);
                        return false;
                    break;

                    case ManagerAd::$DESCRIPTION_ERROR:
                        $this->setSuccess('<span style="color: red;">La description d\'une annonce doit être compris entre 10 et 1000 caractères </span>');
                        $this->deleteDir($path_save, $dir_ad);
                        return false;
                    break;

                    case ManagerAd::$DATE_STOP_ERROR:
                        $this->setSuccess('<span style="color: red;">La date d\'une enchère doit être supérieur à la date actuelle, décalée de 30 secondes </span>');
                        $this->deleteDir($path_save, $dir_ad);
                        return false;
                    break;

                    case ManagerAd::$PRICE_START_ERROR:
                        $this->setSuccess('<span style="color: red;">Le prix d\'une enchère doit être supérieur à 10 centimes </span>');
                        $this->deleteDir($path_save, $dir_ad);
                        return false;
                    break;
                }
                // if ($success == ManagerAd::$SUCCESS) {
                //     $this->setSuccess('<span style="color: green;">Annonce ajoutée</span>');
                //     return true;
                // }
                // if ($)

            }
            
            $this->deleteDir($path_save, $dir_ad);
            $this->setSuccess('<span style="color: red;">Un problème est survenu lors de l\'envoi du formulaire</span>');
            return false;
        }
        return false;
    }

    public function render() {
        // mkdir("Public/pictures/bjhk/", 0777);
        $this->checkNewAd();
        $success = $this->getSuccess();
        // var_dump($success);

        $DB = $this->DB;

        // if ($this->checkNewAd()) { $success = '<span style="color: green;">Annonce ajoutée</span>'; }
      
        $categories = self::getCategories($this->DB);      
        require_once("View/" . $this->template);
    }
}

App::render(__NAMESPACE__, __FILE__, $DB, $config);

?>