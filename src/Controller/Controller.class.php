<?php

namespace App\Controller;

abstract class Controller {
    protected $template;

    public function __construct($template) {
        $this->setTemplate($template);
    }

    public function setTemplate($template) { $this->template = $template; }

    abstract public function render();
}