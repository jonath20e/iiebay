<?php

namespace App\Controller;

use \App\App;

use App\Module\Ad;
use App\Module\Ad\SearchAd;
use App\Model\Bid\BidModel;

class ViewEnchereController extends Controller {
    private $DB;
    private $CONFIG;
    private $PARAM;
    
    public function __construct(\PDO & $DB, ?array & $CONFIG, ?array & $PARAM) {
        $this->DB = $DB;
        $this->CONFIG = $CONFIG;
        $this->PARAM = $PARAM;
        parent::__construct("view-enchere.php");
    }

    public function render() {
        $search = new SearchAd($this->DB);
        if (!isset($this->PARAM["parameters"]["enchere"])) {
            header('Location: ' . $this->CONFIG["Web"]["url"]);
            exit(1);
        }
        $ad = $search->getAd((int) $this->PARAM["parameters"]["enchere"]);
        if (!$ad) {
            header('Location: ' . $this->CONFIG["Web"]["url"]);
            exit(1);
        }
        $offers_m = new BidModel($this->DB);
        $offers = $offers_m->getAllBid($this->PARAM["parameters"]["enchere"]);

        require_once("View/" . $this->template);
    }
}

App::render(__NAMESPACE__, __FILE__, $DB, $config, $PARAM);

?>
    