<?php

namespace App\Controller;

use \App\App;
use App\Module\Category\Traits\TraitManagerCategory;

use App\Module\Category\ManagerCategory;

use App\Module\Ad\Ad;
use App\Module\Ad\SearchAd;

use App\Module\Pagination\Pagination;
use App\Module\Pagination\Template\TemplatePaginationBoot;

use App\Module\Session\Session;

class SearchController extends Controller {
    use TraitManagerCategory;
    private $DB;
    private $CONFIG;
    private $current_page;

    public function __construct(\PDO & $DB, ?array & $CONFIG, ?array & $PARAM) {
        $this->DB = $DB;
        $this->CONFIG = $CONFIG;
        $this->current_page = isset($PARAM["parameters"]["page"]) && (int) $PARAM["parameters"]["page"] > 0 ? $PARAM["parameters"]["page"] : 1;
        parent::__construct("search.php");
    }

    public function render() {
        $DB = $this->DB;

        /**
         * Séléctionne les catégories en utilisant le trait TraitManagerCategory
         * 
         */
        $categories = self::getCategories($this->DB);

        $ads = [];
        
        $search_manager = new SearchAd($DB);

        /**
         * Génération d'un pagination
         */
        $pagination = new Pagination($this->current_page, $this->CONFIG["Search"]["Pagination"]["result_per_page"], $search_manager->countAd(-1, isset($_SESSION["user"]) ? $_SESSION["user"]->getId() : -1));
        $limit = $pagination->displayPage();
       
        /**
         * Vérification formulaire
         */
        if (isset($_POST['name_ad']) && isset($_POST['category']) && isset($_POST["sort"])) {
            SESSION::deleteRunningSession("save_search");

            /**
             * On enregistre les paramètres de recherche
             */
            $save_search = new Session();

            $save_search->addSession("save_search", [
                "name_ad" => $_POST["name_ad"],
                "category" => $_POST['category'],
                "sort" => $_POST["sort"]
            ]);
            $save_search->loadSession();


            /**
             * Redirection
             */
            header('Location: http://localhost/pima/src/search');
            exit(1);
        }
        else {
            $save_search = SESSION::getRunningSession("save_search");
            if ($save_search == null || (isset($_SERVER["HTTP_REFERER"]) && !preg_match('#^http://(www\.)?localhost/pima/src/(search|search/page/[0-9]+)$#', $_SERVER["HTTP_REFERER"]))) {
                $ads  = $search_manager->searchAdPage("", "-1", $limit["min"], $limit["max"], 'priceAsc', -1, isset($_SESSION["user"]) ? $_SESSION["user"]->getPseudo() : '');
                $pagination->setTotalResultCurrentPage(count($ads));
            } 
            else {
                // var_dump($search_manager->countAdSearch($save_search['name_ad'], $save_search['category']));
                $ads  = $search_manager->searchAdPage($save_search['name_ad'], $save_search['category'], $limit["min"], $limit["max"], $save_search["sort"], -1, isset($_SESSION["user"]) ? $_SESSION["user"]->getPseudo() : '');
                $pagination->setTotalResult($search_manager->countAdSearch($save_search['name_ad'], $save_search['category']));
                $pagination->setTotalResultCurrentPage(count($ads));
            }
        }
        
        $sorted = SearchAd::$sorted;

        $template = new TemplatePaginationBoot($this->CONFIG["Web"]["url"] . "search/page/");

        require_once("View/" . $this->template);
    }
}

App::render(__NAMESPACE__, __FILE__, $DB, $config, $PARAM);

?>