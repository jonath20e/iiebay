<?php

namespace App\Controller;

use \App\App;

use App\Model\User\ConnectionUser;
use App\Module\User\ManagerUser;

use App\Module\Session\SessionUser;

class AuthController extends Controller {
    private $DB;
    private $CONFIG;
    
    public function __construct(\PDO & $DB, ?array & $CONFIG, ?array & $PARAM) {
        $this->DB = $DB;
        $this->CONFIG = $CONFIG;
        parent::__construct("connection.php");
    }

    public function render() {
        $error = '';
        if (isset($_POST["mail"]) && isset($_POST["password"])) {
            // $connect = new ConnectionUser($this->DB);
            // echo ManagerUser::hashPassword($_POST["password"], "JDkn6d");
            // $connect->getUser($_POST["mail"], ManagerUser::hashPassword($_POST["password"], "JDkn6d@."));
            $user = ManagerUser::connect($this->DB, $_POST["mail"], $_POST["password"]);
            /**
             * TODO : TRANSFORMER ARRAY USER EN TYPER USER
             */
            // new User(null, $user_info["lastname"], $user_info["firstname"], $user_info["pseudo"], $user_info["mail"], $user_info["password"], $user_info["status"])
            // var_dump($user);
            // exit(1);
            if ($user && ($user->getStatus() == 'user' || $user->getStatus() == 'admin')) {
                (new SessionUser())->openSessionUser($user);
                
                header('Location: index.php');
            } else if ($user && $user->getStatus() == 'ban') 
                $error = '<span style="color: red;">Utilisateur banni</span>';
            else
                $error = '<span style="color: red;">Mauvais identifiants</span>';
        }
             
        require_once("View/" . $this->template);
    }
}

App::render(__NAMESPACE__, __FILE__, $DB, $config);

?>