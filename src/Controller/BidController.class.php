<?php

namespace App\Controller;

use \App\App;

use App\Model\Bid\BidModel;

use App\Module\Pagination\Pagination;
use App\Module\Pagination\Template\TemplatePaginationBoot;

class BidController extends Controller {
    private $DB;
    private $CONFIG;
    private $PRARAM;
    private $current_page;
    
    public function __construct(\PDO & $DB, ?array & $CONFIG, ?array & $PARAM) {
        $this->DB = $DB;
        $this->CONFIG = $CONFIG;
        $this->PARAM = $PARAM;
        $this->current_page = isset($PARAM["parameters"]["page"]) && (int)$PARAM["parameters"]["page"] > 0 ? $PARAM["parameters"]["page"] : 1;
        parent::__construct("bid.php");
    }

    public function render() {
        if (!isset($_SESSION["user"])) {
            header('Location: ' . $this->CONFIG["Web"]["url"]);
            exit(1);
        }

        $bid_model = new BidModel($this->DB);        

        $pagination = new Pagination($this->current_page, $this->CONFIG["Search"]["Pagination"]["result_per_page"], $bid_model->countBid($_SESSION["user"]->getId())["totalBid"]);
        $limit = $pagination->displayPage();

        $result = ($bid_model)->listBid($_SESSION["user"]->getId(), $limit["min"], $limit["max"]);
        $result_all = $result->fetchAll();

        $pagination->setTotalResultCurrentPage(count($result_all));

        $template = new TemplatePaginationBoot($this->CONFIG["Web"]["url"] . "bid/page/");



        require_once("View/" . $this->template);
    }
}

App::render(__NAMESPACE__, __FILE__, $DB, $config, $PARAM);

?>
    