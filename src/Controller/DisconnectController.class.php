<?php

namespace App\Controller;

use \App\App;

use App\Module\Session\Session;

class DisconnectController extends Controller {
    private $DB;
    private $CONFIG;
    
    public function __construct(\PDO & $DB, ?array & $CONFIG, ?array & $PARAM) {
        $this->DB = $DB;
        $this->CONFIG = $CONFIG;
        parent::__construct("");
    }

    public function render() {
        SESSION::destroyAllRunningSession();
        header('Location: index.php?disconnect');
        // require_once("View/" . $this->template);
    }
}

App::render(__NAMESPACE__, __FILE__, $DB, $config["UploadImage"]);

?>