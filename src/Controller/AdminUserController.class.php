<?php

namespace App\Controller;

use \App\App;
use App\Module\User\ManagerUser;
use App\Module\Pagination\Pagination;
use App\Module\Pagination\Template\TemplatePaginationBoot;
use App\Model\Bid\BidModel;
use App\Module\Ad\SearchAd;
use App\Module\Ad\ManagerAd;

class AdminUserController extends Controller {
    private $DB;
    private $CONFIG;
    private $PRARAM;

    private $current_page;
    
    public function __construct(\PDO & $DB, ?array & $CONFIG, ?array & $PARAM) {
        $this->DB = $DB;
        $this->CONFIG = $CONFIG;
        $this->PARAM = $PARAM;
        $this->current_page = isset($PARAM["parameters"]["page"]) && (int)$PARAM["parameters"]["page"] > 0 ? $PARAM["parameters"]["page"] : 1;
        parent::__construct("adminUser.php");
    }

    public function render() {
        if (!isset($_SESSION["user"]) || $_SESSION["user"]->getStatus() != 'admin') {
            header('Location:' . $this->CONFIG["Web"]["url"]);
            exit(1);
        }
        $manager_user = new ManagerUser($this->DB);

        if (isset($this->PARAM["parameters"]["ban"])) {
            $manager_user->ban((int) $this->PARAM["parameters"]["ban"]);
            header('Location: ' . $this->CONFIG["Web"]["url"] . 'admin-user');
            exit(1);
        } else if (isset($this->PARAM["parameters"]["unban"])) {
            $manager_user->unban((int) $this->PARAM["parameters"]["unban"]);
            header('Location: ' . $this->CONFIG["Web"]["url"] . 'admin-user');
            exit(1);
        } else if (isset($this->PARAM["parameters"]["info"]) && !isset($this->PARAM["parameters"]["del-ad"])) {
            $search = new SearchAd($this->DB);
            $pagination = new Pagination($this->current_page, $this->CONFIG["Search"]["Pagination"]["result_per_page"], $search->countAd((int) $this->PARAM["parameters"]["info"]));
            $limit = $pagination->displayPage();

            $my_sales = $search->searchAdPage("", "-1", $limit["min"], $limit["max"], "dateStopDesc", (int) $this->PARAM["parameters"]["info"]);
            $pagination->setTotalResultCurrentPage(count($my_sales));

            $template = new TemplatePaginationBoot($this->CONFIG["Web"]["url"] . "bid/page/");

            require_once("View/info-user.php");
            exit(1);
        } else if (isset($this->PARAM["parameters"]["del-ad"]) && isset($this->PARAM["parameters"]["info"])) {
                $id = (int) $this->PARAM["parameters"]["del-ad"];
                (new ManagerAd($this->DB))->delAd((int) $this->PARAM["parameters"]["info"], $id);
                header("Location:" . $this->CONFIG["Web"]["url"] . "admin-user/info/" . (int) $this->PARAM["parameters"]["info"]);
                exit(1);
        }
 
        $pagination = new Pagination($this->current_page, $this->CONFIG["Search"]["Pagination"]["result_per_page"], $manager_user->getCountUser());
        $limit = $pagination->displayPage();

        $users = $manager_user->getAllUser($limit["min"], $limit["max"]);
        $pagination->setTotalResultCurrentPage(count($users));

        $template = new TemplatePaginationBoot($this->CONFIG["Web"]["url"] . "admin-user/page/");
        
        require_once("View/" . $this->template);
    }
}

App::render(__NAMESPACE__, __FILE__, $DB, $config, $PARAM);

?>
    