<?php

namespace App\Controller;

use \App\App;

use App\Module\Ad\SearchAd;
use App\Module\Ad\ManagerAd;

use App\Module\Pagination\Pagination;
use App\Module\Pagination\Template\TemplatePaginationBoot;

class SalesController extends Controller {
    private $DB;
    private $CONFIG;
    private $PRARAM;
    private $current_page;
    
    public function __construct(\PDO & $DB, ?array & $CONFIG, ?array & $PARAM) {
        $this->DB = $DB;
        $this->CONFIG = $CONFIG;
        $this->PARAM = $PARAM;
        $this->current_page = isset($PARAM["parameters"]["page"]) && (int) $PARAM["parameters"]["page"] > 0 ? $PARAM["parameters"]["page"] : 1;
        parent::__construct("sales.php");
    }

    public function render() {
        if (!isset($_SESSION["user"])) {
            header('Location: ' . $this->CONFIG["Web"]["url"]);
            exit(1);
        }

        if (isset($this->PARAM["parameters"]["close"])) {
            $id = (int) $this->PARAM["parameters"]["close"];
            (new managerAd($this->DB))->closeAd($_SESSION["user"]->getId(), $id);
            header("Location:".  $_SERVER['HTTP_REFERER']); 
            exit(1);
        }

        if (isset($this->PARAM["parameters"]["del"])) {
            $id = (int) $this->PARAM["parameters"]["del"];
            (new managerAd($this->DB))->delAd($_SESSION["user"]->getId(), $id);
            header("Location:".  $_SERVER['HTTP_REFERER']); 
            exit(1);
        }

        $search = new SearchAd($this->DB);
        $pagination = new Pagination($this->current_page, $this->CONFIG["Search"]["Pagination"]["result_per_page"], $search->countAd($_SESSION["user"]->getId()));
        $limit = $pagination->displayPage();

        $my_sales = $search->searchAdPage("", "-1", $limit["min"], $limit["max"], "dateStopDesc", $_SESSION["user"]->getId());
        $pagination->setTotalResultCurrentPage(count($my_sales));

        $template = new TemplatePaginationBoot($this->CONFIG["Web"]["url"] . "sales/page/");
        

        // var_dump($this->PARAM);
            
        require_once("View/" . $this->template);
    }
}

App::render(__NAMESPACE__, __FILE__, $DB, $config, $PARAM);

?>
    