<?php

namespace App\Controller;

use \App\App;

use App\Module\User\User;
use App\Module\User\ManagerUser;

use App\Module\Constraint\ConstraintBasic\ConstraintLessEqual;
use App\Module\Constraint\ConstraintMultiple;

use App\Module\Constraint\ConstraintRange\ConstraintRange;
use App\Module\Constraint\ConstraintRange\ConstraintBetween;
use App\Module\Constraint\ConstraintFormat\ConstraintPattern;

class SubscribeController extends Controller {
    private $DB;
    private $CONFIG;

    public function __construct(\PDO & $DB, ?array & $CONFIG, ?array & $PARAM) {
        $this->DB = $DB;
        $this->CONFIG = $CONFIG;
        parent::__construct("subscribe.php");
    }

    public function render() {
        $success = '';
        $manager_user = new ManagerUser($this->DB, new ConstraintMultiple([
            "lastname" =>  new ConstraintBetween([
                    "min" => 2,
                    "max" => 50
                ]),
            "firstname" => new ConstraintBetween([
                    "min" => 2,
                    "max" => 50
                ]),
            "pseudo" => new ConstraintBetween([
                    "min" => 2,
                    "max" => 50
                ]),
            "mail" => new ConstraintPattern("#^[a-zA-Z0-9._-]{1,255}@[a-zA-Z0-9._-]{2,25}\.[a-z]{2,4}$#"),
            "password" => new ConstraintBetween([
                    "min" => 6,
                    "max" => 50
                ]),
            "status" => new ConstraintRange(ManagerUser::$status)
        ]));

        $matches = [
            ManagerUser::$LAST_NAME_ERROR => "nom",
            ManagerUser::$FIRST_NAME_ERROR => "prénom",
            ManagerUser::$PSEUDO_ERROR => "pseudo",
            ManagerUser::$PASSWORD_ERROR => "mot de passe"
        ];

        // echo $manager_user->addNewUser(new User(null, "test", "test", "tesst", "mail@test.fsr", "eddede", "user"));        

        $new_user = ManagerUser::loadUserFromForm("confirmm_password");
        // var_dump($new_user);
        if (!is_null($new_user) && !is_int($new_user)) {
            $new_user->setStatus("user");
            $result = $manager_user->addNewUser($new_user);
            // echo $result;
            /**
             * Erreur de taille des champs
             */
            if (array_key_exists($result, $matches))
                $success = '<span style="color: red;">Le ' . $matches[$result] . ' doit être compris entre 2 et 50 caractères !</span>';
  
            if ($result == ManagerUser::$PSEUDO_ISSET)
                $success = '<span style="color: red;">Le pseudo existe déjà !</span>';

            if ($result == ManagerUser::$MAIL_ISSET)
                $success = '<span style="color: red;">Le mail existe déjà !</span>';
            
            if ($result == ManagerUser::$SUCCESS) {
                $success = '<span style="color: green;">Le compte a été créé avec succès !</span>';
                $_POST = [];
            }

      
        }
        
        if (is_int($new_user) && $new_user == ManagerUser::$CONFIRM_PASSWORD_ERROR)
            $success .= '<br /><span style="color: red;">Erreur dans la confirmation de mot de passe</span>';

        

        require_once("View/" . $this->template);
    }
}

App::render(__NAMESPACE__, __FILE__, $DB, $config);