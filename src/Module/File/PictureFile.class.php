<?php

namespace App\Module\File;

class PictureFile extends File {

    public static $EXTENSIONS = [
        "jpg",
        "jpeg",
        "png",
        "bmp",
        "gif"
    ];

    public function __construct(string $path, ?string $path_upload, ?int $size = null) {
        parent::__construct($path, $path_upload, $size);
        if (!self::isPicture($this)) { $this->path = null; }
    }
    
    public static function isPicture(File $file) { in_array($file->getExtension(), self::$EXTENSIONS); } 

    public function setExtension(string $extension) {
        if (self::isPicture($this))
            $this->extension = strtolower(trim($extension));
    }


}