<?php

namespace App\Module\File;

class File {
    /**
     * Name and dirname of file
     *
     * @var string
     */
    protected $path;

    protected $name;

    protected $dirname;

    protected $extension;

    protected $size;

    protected $path_upload;

    public function __construct(string $path, ?string $path_upload = null, ?int $size = null) {
        $this->setPath($path);

        $this->setName(strstr(basename(($this->getPath() != null) ? $this->getPath() : ""), '.', true));
        $this->setDirname(dirname(($this->getPath() != null) ? $this->getPath() : ""));
        $this->setExtension(($this->getPath() != null) ? substr(strrchr($this->getPath(),'.'),1) : "");
        $this->setSize($size);

        $this->setPathUpload($path_upload);
    }
    
    public static function loadFromForm(string $index): ?array {
        if (!isset($_FILES[$index])) 
            return null;
                
        // var_dump($_FILES);
        $array_file = [];

        if (!is_array($_FILES[$index]["name"]))
            return [new File("/tmp/" . $_FILES[$index]["name"], $_FILES[$index]["tmp_name"], $_FILES[$index]["size"])];

        $i = 0;

        while (isset($_FILES[$index]["name"][$i]) && $_FILES[$index]["name"][$i] != '') {
            $array_file[] = new File("/tmp/" . $_FILES[$index]["name"][$i], $_FILES[$index]["tmp_name"][$i], $_FILES[$index]["size"][$i]);
            $i++;
        }

        return $array_file;
    } 

    public function getPath(): ?string { return $this->path; }

    public function getDirname(): string { return $this->dirname; }

    public function getName(): ?string { return $this->name; }

    public function getExtension(): string { return $this->extension; }

    public function getSize(): ?int { return $this->size; }

    public function getPathUpload(): ?string { return $this->path_upload; }

    private function setPath(string $path) { $this->path = preg_match('#^([a-zA-Z]+:/{1,2}|/)?([a-zA-Z0-9\-_ ()\']+/{1})+[a-zA-Z0-9-_ ()\']+(\.[a-zA-Z0-9]+)+$#', $path) ? $path : null; }

    public function setName(string $name) { $this->name = $name; }

    public function setDirname(string $dirname) { $this->dirname = $dirname; }

    public function setExtension(string $extension) { $this->extension = strtolower(trim($extension)); }

    public function setSize(?int $size) { $this->size = $size; }

    public function setPathUpload(?string $path_upload) { $this->path_upload = $path_upload; }

}