<?php

namespace App\Module\Category;

use App\Model\Category\ManagerCategoryModel;

class ManagerCategory {
    private $category_model;

    public function __construct(\PDO $db) { 
        $this->category_model = new ManagerCategoryModel($db); 
    }

    public function getAllCategory(): array {
        $list_categories = [];
        $cursor = $this->category_model->getAllCategory();
        while ($current = $cursor->fetch()) 
            $list_categories[] = new Category($current["name"], $current["id"]);

        return $list_categories;
    }

    /** TODO */
    public function newCategory($name): boolval {

    } 

}
