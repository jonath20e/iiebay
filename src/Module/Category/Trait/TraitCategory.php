<?php

namespace App\Module\Category\Traits;

use App\Module\Category\ManagerCategory;

trait TraitManagerCategory {
    public static function getCategories($DB) {
        $m_category = new ManagerCategory($DB);

        return $m_category->getAllCategory();   
    }
}