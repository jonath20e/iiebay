<?php

namespace App\Module\Category;

class Category {
    private $id;

    private $name;

    public function __construct(string $name, ?int $id = null) {
        $this->setName($name);
        $this->setid($id);
    }

    public function setId(?int $id) { $this->id = $id; }

    public function setName(string $name) { $this->name = $name; }

    public function getId(): ?int { return $this->id; }

    public function getName(): string { return $this->name; } 
}