<?php

namespace App\Module\Constraint;

abstract class Constraint {
    private $data_contrainst;
    private $id_contrainst;

    abstract public function __construct($data_contrainst, int $id_contrainst = 0);

    abstract public function isValid($data_check): bool;

    abstract public function setDataConstraint($data_contrainst);

    public function setIdContrainst(int $id_contrainst) { $this->id_contrainst = ($id_contrainst >= 0) ? $id_contrainst : 0; }

    public function getDataConstraint() { return $this->data_contrainst; }

    public function getIdContrainst(): int { return $this->id_contrainst; }
}