<?php

namespace App\Module\Constraint\ConstraintBasic;

use App\Module\Constraint\Constraint;

class ConstraintEqual extends Constraint {
    public function __construct($data_contrainst, int $id_contrainst = 0) {
        $this->setDataConstraint($data_contrainst);
        $this->setIdContrainst($id_contrainst);
    }

    public function isValid($data_check): bool { return $data_check == $this->data_contrainst; }

    public function setDataConstraint($data_contrainst) { $this->data_contrainst = $data_contrainst; }
}