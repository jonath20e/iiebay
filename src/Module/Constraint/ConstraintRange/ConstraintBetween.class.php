<?php

namespace App\Module\Constraint\ConstraintRange;

use App\Module\Constraint\Constraint;

class ConstraintBetween extends Constraint {
    public function __construct($data_contrainst, int $id_contrainst = 0) {
        $this->setDataConstraint($data_contrainst);
        $this->setIdContrainst($id_contrainst);
    }

    public function isValid($data_check): bool { return (is_string($data_check) ? strlen($data_check) : $data_check)  >= $this->data_contrainst["min"] && (is_string($data_check) ? strlen($data_check) : $data_check) <= $this->data_contrainst["max"]; }

    private function isCorrect($data_contrainst) { return is_array($data_contrainst) && array_key_exists("min", $data_contrainst) && array_key_exists("max", $data_contrainst); }

    public function setDataConstraint($data_contrainst) { 
        if ($this->isCorrect($data_contrainst))
            $this->data_contrainst = $data_contrainst; 
    }
}