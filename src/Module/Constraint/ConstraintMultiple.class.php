<?php

namespace App\Module\Constraint;

class ConstraintMultiple {
    private static $TYPE_ERROR = -1;
    private static $SUCCESS = -2;

    private $constraints;

    public function __construct(array $constraints) {
        $this->setconstraints($constraints);
    }

    public function getConstraints(): array { return $this->constraints; }

    public function setConstraints(array $constraints) {
        $stop = false;
        foreach ($constraints as $contrainst)
            if (!($contrainst instanceof Constraint)) {
                $stop = true;
                break;
            }

        if (!$stop)
            $this->constraints = $constraints;
    }

    public function isAllValid(array $data_check): int {
        foreach ($this->constraints as $key => $contrainst) {
            if (!($contrainst instanceof Contrainst\Contrainst))
                return self::$TYPE_ERROR;
            else if (!$contrainst->isValid($data_check[$key])) 
                return $contrainst->getIdContrainst();
        }

        return self::$SUCCESS;
    }
}