<?php

namespace App\Module\Constraint\ConstraintFormat;

use App\Module\Constraint\Constraint;

class ConstraintPattern extends Constraint {
    public function __construct($data_contrainst, int $id_contrainst = 0) {
        $this->setDataConstraint($data_contrainst);
        $this->setIdContrainst($id_contrainst);
    }

    public function isValid($data_check): bool { return is_string($this->data_contrainst) && preg_match($this->data_contrainst, $data_check); }

    public function setDataConstraint($data_contrainst) { $this->data_contrainst = $data_contrainst; }
}