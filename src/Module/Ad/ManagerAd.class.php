<?php

namespace App\Module\Ad;

use App\Model\Ad\ManagerAdModel;
use App\Model\Ad\SearchAdModel;

use App\Module\Constraint\ConstraintMultiple;

class ManagerAd {
    public static $NAME_ERROR = -1;
    public static $DESCRIPTION_ERROR = -2;
    public static $DATE_STOP_ERROR = -3;
    public static $PRICE_START_ERROR = -4;
    // public static $CATEGORY_ERROR = -5;
    public static $ADD_AD_ERROR = -8;

    public static $SUCCESS = -10;

    private $ad_model;
    private $constraints;

    public function __construct(\PDO $db, ?ConstraintMultiple $constraints = null) { 
        $this->ad_model = new ManagerAdModel($db); 
        if (!is_null($constraints))
            $this->setConstraint($constraints);
    }

    public function newAd(Ad $ad): int {
        $result = $this->isCorrect($ad);
        if ($result != self::$SUCCESS)
            return $result;

        return ($this->ad_model->newAd($ad) == 1 ? self::$SUCCESS : self::$ADD_AD_ERROR);
    }

    public function closeAd(int $id_user, int $îd_ad): bool {
        return $this->ad_model->closeAd($id_user, $îd_ad) == 1;
    }

    public function delAd(int $id_user, int $îd_ad) {
        return $this->ad_model->delAd($id_user, $îd_ad) == 1;
    }

    public function setConstraint(ConstraintMultiple $constraints) { 
        if (array_key_exists("name", $constraints->getConstraints()) && 
            array_key_exists('description', $constraints->getConstraints()) && 
            array_key_exists('date_stop', $constraints->getConstraints()) && 
            array_key_exists('price_start', $constraints->getConstraints())
            // array_key_exists('category', $constraints->getConstraints())
        ) {
            $this->constraints = $constraints;
        }
     }

    public function isCorrect(Ad $ad): int {
        $constraints = $this->constraints;

        if ($constraints != [] && $constraints->getConstraints() != []) {
            if (!$constraints->getConstraints()["name"]->isValid(trim($ad->getName())))
                return self::$NAME_ERROR;
            
            if (!$constraints->getConstraints()["description"]->isValid(trim($ad->getDescription())))
                return self::$DESCRIPTION_ERROR;
            
            if (!$constraints->getConstraints()["date_stop"]->isValid(strtotime($ad->getDateStop())))
                return self::$DATE_STOP_ERROR;
            
            if (!$constraints->getConstraints()["price_start"]->isValid($ad->getPriceStart()))
                return self::$PRICE_START_ERROR;
            
            // if (!$constraints->getConstraints()["category"]->isValid($ad->getCategory()))
            //     return self::$CATEGORY_ERROR;
        }

        return self::$SUCCESS;
    }
}
