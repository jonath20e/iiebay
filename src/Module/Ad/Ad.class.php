<?php

namespace App\Module\Ad;

use App\Module\Category\Category;

class Ad {
    private $id;

    private $current_price;

    private $id_user;
    /**
     * Undocumented variable
     *
     * @var [type]
     */
    private $name;


    /**
     * Undocumented variable
     *
     * @var [type]
     */
    private $category;


    /**
     * Undocumented variable
     *
     * @var [type]
     */
    private $price_start;

    private $description;

    private $picture;

    private $album;

    private $date_stop;

    private $status;

    public function __construct(string $name, float $price_start, string $date_stop, ?string $description = null, ?string $picture, ?string $album, ?Category $category = null, ?int $id = null, ?int $id_user = null, ?float $current_price = null, ?string $status = 'open') {
        $this->setName($name);
        $this->setPriceStart($price_start);
        $this->setDateStop($date_stop);
        $this->setDescription($description);
        $this->setPicture($picture);
        $this->setAlbum($album);
        $this->setCategory($category);
        $this->setId($id);
        $this->setIdUser($id_user);
        $this->setCurrentPrice($current_price);
        $this->setStatus($status);
    }

    public function setName(string $name) { $this->name = trim($name); }

    public function setPriceStart(float $price_start) { $this->price_start = ($price_start > 0) ? (float) (floor($price_start * 100) / 100) : 0; }

    public function setDateStop(string $date_stop) {
        // if (preg_match(''))
        // TODO FAIRE VERIFICATION
        $this->date_stop = trim($date_stop);
    }

    public function setDescription(?string $description) { $this->description = trim($description); }

    public function setPicture(?string $picture) { $this->picture = trim($picture); }
    
    public function setAlbum(?string $album) { $this->album = trim($album); }

    public function setCategory(?Category $category) { $this->category = $category; }

    public function setId(?int $id) { $this->id = $id; }

    public function setIdUser(?int $id_user) { $this->id_user = $id_user; }

    public function setCurrentPrice(?float $current_price) { $this->current_price = $current_price; }

    public function setStatus(?string $status) { $this->status = $status; }
    
    public function getName(): string { return $this->name; }

    public function getPriceStart(): float { return $this->price_start; }

    public function getDateStop(): string { return $this->date_stop; }

    public function getDescription(): ?string { return $this->description; }

    public function getPicture(): ?string { return $this->picture; }

    public function getAlbum(): ?string { return $this->album; }

    public function getCategory(): ?Category { return $this->category; }

    public function getId(): ?int { return $this->id; }

    public function getIdUser(): ?int { return $this->id_user; }

    public function getCurrentPrice(): ?float { return $this->current_price; }

    public function getStatus(): ?string { return $this->status; }
}