<?php

namespace App\Module\Ad;

use App\Model\Ad\SearchAdModel;

class SearchAd {
    private $ad_model_search;
    public static $sorted = [
        "priceAsc" => [
            "sql" => "ORDER BY current_price",
            "text" => "Trier par prix croissant"
        ],
        "priceDesc" => [
            "sql" => "ORDER BY current_price DESC",
            "text" => "Trier par prix décroissant"
        ],
        "dateStopAsc" => [
            "sql" => "ORDER BY date_stop",
            "text" => "Trier par date de fin d'enchère croissant"
        ],
        "dateStopDesc" => [
            "sql" => "ORDER BY date_stop DESC",
            "text" => "Trier par date de fin d'enchère décroissant"
        ]
    ];

    public function __construct(\PDO $db) { 
        $this->ad_model_search = new SearchAdModel($db);
    }

    public function searchAdAll(string $word, string $category): array {
        $result_search = ($this->ad_model_search)->searchAd($word, $category);

        return $this->generateAdsPDO($result_search);
    }

    public function searchAdPage(string $word, string $category, int $min, int $max, string $sort = "priceAsc", int $id_user = -1, string $pseudo_diff = ''): array {
        $final_sort = (isset(self::$sorted[$sort])) ? self::$sorted[$sort]["sql"] : self::$sorted["priceAsc"]["sql"];
        // var_dump($final_sort);
        $result_search = ($this->ad_model_search)->searchAdPerPage($word, $category, $min, $max, $final_sort, $id_user, $pseudo_diff);

        return $this->generateAdsPDO($result_search);
    }

    public function getAd(int $id): ?array {
        $ad = $this->ad_model_search->getAd($id);

        return !is_null($ad) && $ad != [] ? [
            "ad" => new Ad($ad["name"], $ad["price_unit_start"], $ad["date_stop"], $ad["description"], $ad["picture"], $ad["album"], null, $ad["id"], $ad["id_user"], $ad["current_price"], $ad["status_ad"]),
            "pseudo" => $ad["pseudo"],
            "mail" => $ad["mail"]
        ] : null;
    }

    public function countAd(int $id_user = -1, int $id_user_diff = -1): ?int {
        $count = $this->ad_model_search->countAd($id_user, $id_user_diff);
        // var_dump($count);

        return (!is_null($count) && isset($count["totalAd"]) ? $count["totalAd"] : null);
    }

    public function countAdSearch(string $word, string $category): ?int {
        $count = $this->ad_model_search->countAdSearch($word, $category);
        // var_dump($count);

        return (!is_null($count) && isset($count["totalAd"]) ? $count["totalAd"] : null);
    }

    private function generateAdsPDO($result_search) {
        $ads = [];
        $i = 0;

        while ($current = ($result_search->fetch())) {
            $ads[]["ad"] = new Ad($current["name"], $current["price_unit_start"], $current["date_stop"], $current["description"], $current["picture"], $current["album"], null, $current["id"], $current["id_user"], $current["current_price"], $current["status_ad"]);
            $ads[$i]["pseudo"] = $current["pseudo"];

            $i++;
        }
        
        return $ads;
    }

}