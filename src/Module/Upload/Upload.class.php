<?php

namespace App\Module\Upload;

use App\Module\Constraint\ConstraintMultiple;

use App\Module\File\File;

class ManagerUpload {
    public static $FILE_ERROR = -1;
    public static $SIZE_ERROR = -2;
    public static $EXTENSION_ERROR = -3;
    
    public static $SUCCESS = -10;

    private $file;
    private $path_save;
    private $constraint;

    public function __construct(string $path_save) {
        $this->setPathSave($path_save);
        $this->constraint = null;
        // $this->setFile($file);
        // $this->set
    }

    public function getFile(): File { return $this->file; }

    public function getPathSave(): string { return $this->path_save; }

    public function getConstraint(): ?array { return $this->constraint; }

    public function setFile(File $file) { $this->file = $file; }
    
    public function setPathSave(string $path_save) { $this->path_save = $path_save; }

    public function setConstraint(ConstraintMultiple $constraint) {
        if (array_key_exists("size", $constraint->getConstraints()) && 
            array_key_exists('extension', $constraint->getConstraints())
        ) {
            $this->constraint = $constraint;
        }
    }

    public function upload(File $file): int {
        if (!file_exists($this->getPathSave()))
            return self::$FILE_ERROR;
        
        $constraints = $this->constraint;

        if ($constraints != [] && $constraints->getConstraints() != []) {
            if (!$constraints->getConstraints()["size"]->isValid($file->getSize()))
                return self::$SIZE_ERROR;
            
            if (!$constraints->getConstraints()["extension"]->isValid($file->getExtension()))
                return self::$EXTENSION_ERROR;
            
        }


        if (move_uploaded_file($file->getPathUpload(), $this->getPathSave() . $file->getName() . '.' .$file->getExtension()))
            return self::$SUCCESS;
    } 
}