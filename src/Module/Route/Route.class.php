<?php

namespace App\Module\Route;

class Route {
  /**
   * Intitulé du chemin
   * @var string
   */
  private $title;


  /**
   * Chemin relatif de la route
   * @var string
   */
  private $path;


  /**
   * __construct
   * @param string $title intitulé
   * @param string $path  chemin relatif
   */
  public function __construct($title, $path) {
    $this->title = $title;
    $this->path = $path;
  }


  /**
   * Récupére chemin
   * @return string
   */
  public function getPath() { return $this->path; }


  /**
   * Récupère intitulé
   * @return string
   */
  public function getTitle() { return $this->title; }
}


 ?>
