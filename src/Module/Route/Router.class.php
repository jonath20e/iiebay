<?php

namespace App\Module\Route;

use App\Module\Route\Route;

class Router {
  /**
   * Tableau d'objet route
   * @var array
   */
  private $routes;


  /**
   * Route de la gestion d'erreur
   *
   * Lorsque une route est introuvable, $route404 est invoqué
   *
   * @var Route
   */
  private $route404;


  /**
   * Tableau de toutes les routes qui sont mis en marche
   * @var [type]
   */
  private $routesRun;


  /**
   * __construct
   */
  public function __construct() {
    $routes = [];
    $route404 = null;
    $routesRun = [];
  }


  /**
   * Ajoute une nouvelle route
   * @param Route $route
   */
  public function add(Route $route) { $this->routes[] = $route; }


  /**
   * Configure la route d'erreur
   * @param Route $route404 [description]
   */
  public function setRoute404(Route $route404) { $this->route404 = $route404; }


  /**
   * Met en marche les routes précédemmment ajoutées
   * @return null
   */
  public function run() {
    foreach ($this->routes as $route){
      $this->routesRun[$route->getTitle()]['route'] = $route;
      $this->routesRun[$route->getTitle()]['path'] = $route->getPath();
    }
  }


  /**
   * Récupère les paramètres de l'url en fonction du délimiteur
   * 
   * Exemple : http://site.com/route/parametre1/contenu1/parametre2/contenu2 pour 
   * $delimiter = '/'
   * 
   * @param  string $url
   * @param  string $delimiter
   * @return array
   */
  public static function getParameters($url, $delimiter = '/') {
    $url_array = explode($delimiter, $url);

    $parameters = [];

    $title = array_shift($url_array);
    $parameters['title'] = $title;

    $n = count($url_array);

    for ($i = 0; $i < $n; $i += 2)
      $parameters['parameters'][$url_array[$i]] = $url_array[$i + 1] ?? null;

    return $parameters;
  }


  /**
   * Récupère toutes les infos d'un route suivant l'url et le délimiteur
   * @param  string $url
   * @param  string $delimiter
   * @return array
   */
  public function getInfoRoute($url, $delimiter = '/') {
    $paramaters = self::getParameters($url, $delimiter);

    if (!isset($this->routesRun[$paramaters['title']]))
      return ['path' => ($this->route404 != null ? $this->route404->getPath() : null)];

    $this->routesRun[$paramaters['title']]['parameters'] = $paramaters;
    return $this->routesRun[$paramaters['title']];
  }
}


 ?>
