<?php

namespace App\Module\Pagination\Template;

use App\Module\Pagination\ITemplatePagination;

class TemplatePaginationBoot implements ITemplatePagination {
    private $url;

    public function __construct(string $url) {
        $this->setUrl($url);
    }

    public function setUrl(string $url) { $this->url = $url; }

    public function getUrl(): string { return $this->url; }

    public function TopHeader(): string { return '<ul class="pagination pagination-lg justify-content-center">'; }

    public function BottomHeader(): string { return '</ul>'; }

    public function getPage(int $current_page, bool $active): string {
        return '<li class="page-item' . (($active) ? " active" : "") . '"><a class="page-link" href="' . $this->getUrl() . $current_page . '">' . $current_page . '</a></li>';
    }

    public function getPageNext(int $page, bool $isEnough): string {
        return '<li class="page-item ' . ($isEnough ? 'disabled' : '') . '"><a class="page-link" href="' . $this->getUrl() . $page . '">Suivant</a></li>';
    }

    public function getPagePrevious(int $page): string {
        return '<li class="page-item ' . ($page == 0 ? 'disabled' : '') . '"><a class="page-link" href="' . $this->getUrl() . $page . '">Précédent</a></li>';
    }

    public function getPageLimit(): string {
        return '<li class="page-item disabled"><a class="page-link" href="#">...</a></li>';
    }
}