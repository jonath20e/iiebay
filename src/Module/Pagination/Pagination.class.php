<?php

namespace App\Module\Pagination;

class Pagination {
    private $current_page;
    private $result_per_pages;
    private $total_result;
    private $cut;

    public function __construct(int $current_page, int $result_per_pages, int $total_result, ?int $total_result_current_page = null, int $cut = 5) {
        $this->setCurrentPage($current_page);
        $this->setResultPerPage($result_per_pages);
        $this->setTotalResult($total_result);
        $this->setTotalResultCurrentPage((is_null($total_result_current_page) ? $result_per_pages : $total_result_current_page));
        $this->setCut($cut);
    }

    public function setCurrentPage(int $current_page) { $this->current_page = ($current_page >= 1 ? $current_page : 1); }

    public function setResultPerPage(int $result_per_pages) { $this->result_per_pages = ($result_per_pages >= 1 ? $result_per_pages : 1); }

    public function setTotalResult(int $total_result) { $this->total_result = ($total_result >= 0 ? $total_result : 1); }

    public function setTotalResultCurrentPage(int $total_result_current_page) { $this->total_result_current_page = $total_result_current_page;}

    public function setCut(int $cut) { $this->cut = $cut; }

    public function getCurrentPage(): int { return $this->current_page; }

    public function getResultPerPage(): int { return $this->result_per_pages; }

    public function getTotalResult(): int { return $this->total_result; }

    public function getTotalResultCurrentPage(): int { return $this->total_result_current_page; }

    public function getCut(): int { return $this->cut; }

    public function displayPage(): array {
        return [
            "min" => ($this->getCurrentPage() - 1) * $this->getResultPerPage(),
            "max" => $this->getCurrentPage() * $this->getResultPerPage()
        ];
    }

    public function isDisplayTemplate(): bool { return (($this->getCurrentPage() * $this->getTotalResultCurrentPage()) <= $this->getTotalResult() && $this->getTotalResultCurrentPage() != $this->getTotalResult()); }

    public function generateTemplate(ITemplatePagination $template): string {
        $render = '';
        
        if ($this->isDisplayTemplate()) {
            $render .= $template->TopHeader();
            $render .= $template->getPagePrevious($this->current_page - 1);

            $decalage = ($this->getCurrentPage() >= $this->getCut() ? $this->getCurrentPage() - $this->getCut() + 1 : 0);
            
            for ($i = 1 + $decalage; $i < (((int) $this->getTotalResult() / $this->getResultPerPage())) + 1; $i++) {
                    if ($i == 1 + $decalage + ($this->getCut())) {
                        $render .= $template->getPageLimit();
                        break;
                    }
                    $render .= $template->getPage($i, $i == $this->getCurrentPage());
            }
            
            $render .= $template->getPageNext($this->current_page + 1, $i == $this->current_page + 1);
        }

        return $render;
    }
}