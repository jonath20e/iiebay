<?php

namespace App\Module\Pagination;

interface ITemplatePagination {
    public function TopHeader(): string;
    public function BottomHeader(): string;
    public function getPage(int $current_page, bool $active): string;
    public function getPageNext(int $page, bool $isEnough): string;
    public function getPagePrevious(int $page): string;
    public function getPageLimit(): string;
}