<?php

namespace App\Module\User;

class User {
    private $id;
    private $lastname;
    private $firstname;
    private $pseudo;
    private $mail;
    private $password;
    private $status;

    public function __construct(?int $id, string $lastname, string $firstname, string $pseudo, string $mail, ?string $password, ?string $status) {
        $this->setId($id);
        $this->setLastName($lastname);
        $this->setFirstName($firstname);
        $this->setPseudo($pseudo);
        $this->setMail($mail);
        $this->setPassword($password);
        $this->setStatus($status);
    }

    public function setId(?int $id) { $this->id = $id; }

    public function setLastName(string $lastname) { $this->lastname = trim($lastname); }

    public function setFirstName(string $firstname) { $this->firstname = trim($firstname); }

    public function setPseudo(string $pseudo) { $this->pseudo = trim($pseudo); }

    public function setMail(string $mail) { $this->mail = trim($mail); }

    public function setPassword(string $password) { $this->password = trim($password); }

    public function setStatus(?string $status) { $this->status = trim($status); }

    public function getId(): ?int { return $this->id; }

    public function getLastName(): string { return $this->lastname; }

    public function getFirstName(): string { return $this->firstname; }

    public function getPseudo(): string { return $this->pseudo; }

    public function getMail(): string { return $this->mail; }

    public function getPassWord(): ?string { return $this->password; }

    public function getStatus(): ?string { return $this->status; }

}