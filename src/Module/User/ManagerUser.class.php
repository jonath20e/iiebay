<?php

namespace App\Module\User;

use App\Module\Constraint\ConstraintMultiple;

use App\Model\User\ManagerUser as ManagerUserModel;
use App\Model\User\ConnectionUser;

use App\Module\Session\Session;

class ManagerUser {
    public static $LAST_NAME_ERROR = -1;
    public static $FIRST_NAME_ERROR = -2;
    public static $PSEUDO_ERROR = -3;
    public static $MAIL_ERROR = -4;
    public static $PASSWORD_ERROR = -5;
    public static $STATUS_ERROR = -6;
    public static $CONFIRM_PASSWORD_ERROR = -7;
    public static $ADD_USER_ERROR = -8;

    public static $SUCCESS = -10;

    public static $PSEUDO_ISSET = -11;
    public static $MAIL_ISSET = -12;

    public static $status = [
        "user",
        "admin",
        "ban",
        "timeout"
    ];

    // private $error; Prévoir plus tard de créer une méthode getError()
    private $constraints;
    private $user_model;
    private $DB;

    public static function issetPseudo(\PDO & $DB, string $pseudo): bool {
        $id = (new ManagerUserModel($DB))->issetPseudo($pseudo);
       return isset($id) && !is_null($id) && isset($id["id"]);
    }

    public static function issetMail(\PDO & $DB, string $mail): bool {
        $id = (new ManagerUserModel($DB))->issetPseudo($mail);
        return isset($id) && !is_null($id) && isset($id["id"]);
    }

    public static function connect(\PDO & $DB, string $mail, string $password, bool $hash = true): ?User {
        $connect = new ConnectionUser($DB);
        $user = $connect->getUser($mail, ($hash ? self::hashPassword($password, "JDkn6d@.") : $password));
        
        return (!is_null($user) && $user != false ? new User($user["id"], $user["lastname"], $user["firstname"], $user["pseudo"], $user["mail"], $user["password"], $user["status"]) : null);
    }

    /**
     * Vérifie si l'utilisateur est connctée ou non
     *
     * @return User|null
     */
    public static function loadCurrentUser(): ?User {
        return SESSION::getRunningSession("user");
    }

    /**
     * Crée un user à partir d'un formulaire d'inscription complet
     *
     * @return User|null|int
     */
    public static function loadUserFromForm(?string $confirmm_password) {
        if (isset($_POST["lastname"]) && isset($_POST["firstname"]) && isset($_POST["pseudo"]) && isset($_POST["mail"]) && isset($_POST["password"])) {
            if (!is_null($confirmm_password) && (!isset($_POST[$confirmm_password]) || $_POST[$confirmm_password] != $_POST["password"]))
                return self::$CONFIRM_PASSWORD_ERROR;
                
            if (isset($_POST["status"]) && in_array($_POST["status"], self::$status))
                return new User(null, $_POST["lastname"], $_POST["firstname"], $_POST["pseudo"], $_POST["mail"], $_POST["password"], $_POST["status"]);
            return new User(null, $_POST["lastname"], $_POST["firstname"], $_POST["pseudo"], $_POST["mail"], $_POST["password"], null);
        }
        return null;
    }

    public static function hashPassword(string $password, string $salt): string {
        return hash("sha256", $salt . trim($password) . $salt . hash("sha256", "$password hachi parmentier $salt"));
    }

    /**
     * Undocumented function
     *
     * @param \PDO $DB
     * @param ConstraintMultiple|null $constraints
     */
    public function __construct(\PDO & $DB, ?ConstraintMultiple $constraints = null) {
        $this->DB = $DB;
        $this->user_model = new ManagerUserModel($DB);
        if (!is_null($constraints))
            $this->setConstraint($constraints);
    }


    public function setConstraint(ConstraintMultiple $constraints) { 
        if (array_key_exists("lastname", $constraints->getConstraints()) && 
            array_key_exists('firstname', $constraints->getConstraints()) && 
            array_key_exists('pseudo', $constraints->getConstraints()) && 
            array_key_exists('mail', $constraints->getConstraints()) && 
            array_key_exists('password', $constraints->getConstraints()) && 
            array_key_exists('status', $constraints->getConstraints())
        ) {
            $this->constraints = $constraints;
        }
     }

    public function addNewUser(User $user): int {
        $result = $this->isCorrect($user);
        if ($result != self::$SUCCESS)
            return $result;
        $user->setPassword(self::hashPassword($user->getPassWord(), "JDkn6d@."));

        return ($this->user_model->addUser($user) == 1 ? self::$SUCCESS : self::$ADD_USER_ERROR);        
    }

    public function updateUser(User $old_user, User $user): int {
        $result = $this->isCorrectUpdate($old_user, $user);
        if ($result != self::$SUCCESS)
            return $result;
        $user->setPassword(self::hashPassword($user->getPassWord(), "JDkn6d@."));

        return ($this->user_model->changeProfil($user) == 1 ? self::$SUCCESS : self::$ADD_USER_ERROR);
    }

    public function getAllUser(int $min, int $max, string $sort = ""): array {
        return $this->user_model->getAllUser($min, $max, $sort);
    }

    public function getCountUser() {
        $count = $this->user_model->getCountUser();
        return isset($count["totalUser"]) ? $count["totalUser"] : 0;
    }

    public function ban(int $id): bool {
        return ($this->user_model->ban($id) == 1);
    }

    public function unban(int $id) {
        return ($this->user_model->unban($id) == 1);
    }


    /**
     * Vérifie si un User est correct selon les contraintes reçues
     *
     * @param User $user
     * @return integer
     */
    public function isCorrect(User $user): int {
        $constraints = $this->constraints;

        if ($constraints != [] && $constraints->getConstraints() != []) {
            if (!$constraints->getConstraints()["lastname"]->isValid(trim($user->getLastName())))
                return self::$LAST_NAME_ERROR;
            
            if (!$constraints->getConstraints()["firstname"]->isValid(trim($user->getFirstName())))
                return self::$FIRST_NAME_ERROR;
            
            if (!$constraints->getConstraints()["pseudo"]->isValid(trim($user->getPseudo())))
                return self::$PSEUDO_ERROR;

            if (self::issetPseudo($this->DB, trim($user->getPseudo())))
                return self::$PSEUDO_ISSET;
            
            if (!$constraints->getConstraints()["mail"]->isValid(trim($user->getMail())))
                return self::$MAIL_ERROR;

            if (self::issetMail($this->DB, trim($user->getMail())))
                return self::$MAIL_ISSET;
            
            if (!$constraints->getConstraints()["password"]->isValid(trim($user->getPassWord())))
                return self::$PASSWORD_ERROR;
            
            if (!$constraints->getConstraints()["status"]->isValid(trim($user->getStatus())))
                return self::$STATUS_ERROR;

        }

        return self::$SUCCESS;
    }

    public function isCorrectUpdate(User $old_user, User $user): int {
        $constraints = $this->constraints;

        if ($constraints != [] && $constraints->getConstraints() != []) {
            if (!$constraints->getConstraints()["lastname"]->isValid(trim($user->getLastName())))
                return self::$LAST_NAME_ERROR;
            
            if (!$constraints->getConstraints()["firstname"]->isValid(trim($user->getFirstName())))
                return self::$FIRST_NAME_ERROR;
            
            if (!$constraints->getConstraints()["pseudo"]->isValid(trim($user->getPseudo())))
                return self::$PSEUDO_ERROR;

            if ($old_user->getPseudo() != $user->getPseudo() && self::issetPseudo($this->DB, trim($user->getPseudo())))
                return self::$PSEUDO_ISSET;
            
            if (!$constraints->getConstraints()["mail"]->isValid(trim($user->getMail())))
                return self::$MAIL_ERROR;

            if ($old_user->getMail() != $user->getMail() && self::issetMail($this->DB, trim($user->getMail())))
                return self::$MAIL_ISSET;
            
            if ($user->getPassWord() != '' && !$constraints->getConstraints()["password"]->isValid(trim($user->getPassWord())))
                return self::$PASSWORD_ERROR;
            
            if (!$constraints->getConstraints()["status"]->isValid(trim($user->getStatus())))
                return self::$STATUS_ERROR;

        }

        return self::$SUCCESS;
    }
}