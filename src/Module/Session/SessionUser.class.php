<?php

namespace App\Module\Session;

use App\Module\Session\Session;
use App\Module\User\User;

class SessionUser extends Session {

  public function __construct() {
    parent::__construct();
  }

  public function openSessionUser(User $user) {
    $this->addSession("user", $user);
    $this->loadSession();
  }
}