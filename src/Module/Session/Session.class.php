<?php

namespace App\Module\Session;

class Session {
  protected $sessions;

  public function __construct() {
    $this->sessions = [];
  }

  public static function start() { session_start(); }

  public static function issetRunningSession(string $name_session): bool { return (isset($_SESSION[$name_session])); }

  public static function getRunningSession(string $name_session) {
    if (!self::issetRunningSession($name_session))
      return null;

    return $_SESSION[$name_session];
  }

  public static function destroyAllRunningSession() {
    // session_start();

    $_SESSION = [];
    session_unset();
    session_destroy();
  }

  public static function deleteRunningSession(string $name) {
    if (self::issetRunningSession($name))
      $_SESSION[$name] = null;
  }

  public function addSession($name, $value) {
    $this->sessions[] = [
      "name" => $name,
      "value" => $value
    ];
  }

  public function loadSession() {
    foreach ($this->sessions as $session)
      $_SESSION[$session["name"]] = $session["value"];
  }
}