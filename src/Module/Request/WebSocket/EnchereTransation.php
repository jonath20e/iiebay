<?php
namespace App\Module\Request;

use Ratchet\MessageComponentInterface;
use Ratchet\ConnectionInterface;

use App\Module\User\User;
use App\Module\User\ManagerUser;

use App\Model\Ad\Ad;
use App\Module\Ad\SearchAd;

use App\Model\Bid\BidModel;

use App\Model\PDOConnect;

class EnchereTransation implements MessageComponentInterface {
    protected $clients;

    private $DB;

    public function __construct() {
        $this->clients = new \SplObjectStorage;
        $config = require_once(getcwd() . '/src/config.php');
        
        try {  
             $DB_PDO = new PDOConnect($config);
        }catch (\Exception $e) {
                var_dump($e->getMessage());
                die('Erreur: database connection has failed');
            }
         $this->DB = $DB_PDO->getDB();
    }

    public function onOpen(ConnectionInterface $conn) {
        // Store the new connection to send messages to later
        $this->clients->attach($conn);

        echo "New connection! ({$conn->resourceId})\n";
    }

    public function onMessage(ConnectionInterface $from, $msg) {
        $numRecv = count($this->clients) - 1;
        print "-----------------------------------------------------------------------" . PHP_EOL;
        echo sprintf('Connection %d sending message "%s" to %d other connection%s' . "\n"
            , $from->resourceId, $msg, $numRecv, $numRecv == 1 ? '' : 's');

        print "Request received -> " . $msg . PHP_EOL;
        $data = json_decode($msg);

        
        $code = $data->code;
        if (isset($code->type)) {
            if ($code->type == "sendEnchere") {
                print "Request : sendEnchere" . PHP_EOL;
                if (isset($code->id) && isset($code->value)) {
                    if (isset($data->mail) && isset($data->token) && isset($data->code)) {
                        $user = ManagerUser::connect($this->DB, $data->mail, $data->token, false);
                        if ($user != null) {
                            print "Connection of use id (" . $user->getId() . ")" . PHP_EOL;
                            $search = new SearchAd($this->DB);
                            $ad = $search->getAd($code->id);
                            
                            if ($code->value <= $ad["ad"]->getCurrentPrice()) {
                                $from->send(json_encode([
                                    "error" => "value not enought"
                                ]));
                            }else if ($user->getId() == $ad["ad"]->getIdUser()) {
                                $from->send(json_encode([
                                    "error" => "same user"
                                ]));
                            }else if ($ad["ad"]->getStatus() =='close') {
                                 $from->send(json_encode([
                                    "error" => "close"
                                ]));
                            }else {
                                try {  
                                    $bid = new BidModel($this->DB);

                                    $this->DB->setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION);

                                    $this->DB->beginTransaction();

                                    $total = 0;

                                    $total += $bid->updateCurrentPrice($ad["ad"]->getId(), $code->value);
                                    $total += $bid->addNewBid($ad["ad"]->getId(), $user->getId(), $code->value);
                                    
                                    if ($total != 2) {
                                        $this->DB->rollBack();
                                        print "Transation failed" . PHP_EOL;
                                        $from->send(json_encode([
                                            "error" => "system error"
                                        ]));
                                    }else {
                                         $this->DB->commit();

                                    $from->send(json_encode([
                                            "success" => "transaction done"
                                        ]));
                                    }
                                    $this->resetTransation($code->value, $user);


                                    }catch (\Exception $e) {
                                        $this->DB->rollBack();
                                        print "Transation failed" . PHP_EOL;
                                        $from->send(json_encode([
                                            "error" => "system error"
                                        ]));
                                    }
                            }
                        }
                    }
                }
            }else if ($code->type == "getEnchere") {
                print "Request : getEnchere" . PHP_EOL;
                if (isset($code->id))
                    $ad = $search->getAd($code->id);
            }
        }

        

        // foreach ($this->clients as $client) {
            
        // }
        print "-----------------------------------------------------------------------" . PHP_EOL;
    }

    public function resetTransation(int $value, $user) {
            foreach ($this->clients as $client) {
                $client->send(json_encode([
                    "price" => $value,
                    "histo" => [
                        "pseudo" => $user->getPseudo(),
                        "price" => $value
                    ]
                ]));
            }
    }

    public function onClose(ConnectionInterface $conn) {
        // The connection is closed, remove it, as we can no longer send it messages
        $this->clients->detach($conn);

        echo "Connection {$conn->resourceId} has disconnected\n";
    }

    public function onError(ConnectionInterface $conn, \Exception $e) {
        echo "An error has occurred: {$e->getMessage()}\n";

        $conn->close();
    }
}