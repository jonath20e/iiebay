DROP TABLE IF EXISTS "Category" CASCADE;
DROP TABLE IF EXISTS "Ad" CASCADE;
DROP TABLE IF EXISTS "User" CASCADE;
DROP TABLE IF EXISTS "Bid" CASCADE;

CREATE TABLE "User" (
    id SERIAL PRIMARY KEY NOT NULL,
    lastname VARCHAR(50) NOT NULL,
    firstname VARCHAR(50) NOT NULL,
    pseudo VARCHAR(50) UNIQUE NOT NULL,
    mail VARCHAR(300) UNIQUE NOT NULL,
    -- phone VARCHAR(12),
    password VARCHAR(80) NOT NULL,
    status VARCHAR(50) NOT NULL, -- user / admin / ban / timeout:46465516
    date_timeout TIMESTAMP DEFAULT NULL,
    date_subscribe TIMESTAMP DEFAULT current_timestamp
);

CREATE TABLE "Category" (
    id SERIAL PRIMARY KEY NOT NULL,
    name VARCHAR(50) UNIQUE NOT NULL
);

CREATE TABLE "Ad" (
    id SERIAL PRIMARY KEY NOT NULL,
    id_user INTEGER REFERENCES "User" ON DELETE CASCADE NOT NULL,
    id_category INTEGER REFERENCES "Category" ON DELETE SET NULL,
    name VARCHAR(50) NOT NULL,
    description TEXT DEFAULT NULL,
    price_unit_start FLOAT NOT NULL,
    current_price FLOAT NOT NULL,
    picture VARCHAR(200) DEFAULT NULL, -- chemin relatif à partir d'un chemin prédéfini dans le script php
    album VARCHAR(2000) DEFAULT NULL, -- chemin relatif à partir d'un chemin prédéfini dans le script php
    status_ad VARCHAR(50) DEFAULT 'open',
    date_stop TIMESTAMP NOT NULL,
    date_adding TIMESTAMP DEFAULT current_timestamp    
);

CREATE TABLE "Bid" (
    id SERIAL PRIMARY KEY NOT NULL,
    id_ad INTEGER REFERENCES "Ad" ON DELETE CASCADE NOT NULL,
    id_buyer INTEGER REFERENCES "User" ON DELETE CASCADE NOT NULL,
    price INTEGER NOT NULL,
    date_bid TIMESTAMP DEFAULT current_timestamp
);

INSERT INTO "Category"(name) VALUES
    ('Informatique'),
    ('Consoles et jeux vidéos'),
    ('Image et son'),
    ('Téléphonie'),
    ('DVD - Films'),
    ('CD - Musique'),
    ('Livres'),
    ('Vélos'),
    ('Sports et hobbies'),
    ('Instruments de musique'),
    ('Ameublement'),
    ('Electroménager'),
    ('Décoration'),
    ('Linge de maison'),
    ('Bricolage'),
    ('Jardinage'),
    ('Vêtements'),
    ('Chaussures'),
    ('Accessoires et bagagerie'),
    ('Montres et bijoux'),
    ('Autres');

INSERT INTO "User"(lastname, firstname, pseudo, mail, password, status) VALUES
    ('Kevin', 'Kevin', 'KevinGamer', 'kevin@gmail.com', 'dc457af63c89e6cf38f61705d890e85064d6f5a1045be9df1ddda1517d020249', 'user'),
    ('Admin test', 'Admin test', 'admin', 'admin@gmail.com', 'dc457af63c89e6cf38f61705d890e85064d6f5a1045be9df1ddda1517d020249', 'admin'),
    ('BOAKNIN', 'Jonathan', 'John', 'jonath20e@gmail.com', 'dc457af63c89e6cf38f61705d890e85064d6f5a1045be9df1ddda1517d020249', 'user'),
    ('Dupond', 'Durand', 'Dupont', 'machin@gmail.com', 'dc457af63c89e6cf38f61705d890e85064d6f5a1045be9df1ddda1517d020249', 'user');
INSERT INTO "Ad"(id_user, id_category, name, description, price_unit_start, current_price, picture, date_stop, date_adding) VALUES
    (1, 7, 'Saga Harry Potter', 'toute la collection', 50, 50, 'Public/pictures/1315/4654655/pictureAd.jpg', '2018-10-20', NOW()),
    (1, 1, 'PC Portable MSI', 'PC pour gamer', 680, 680, 'Public/pictures/1315/165151615/pictureAd.jpg', '2018-10-22', NOW()),
    (4, 19, 'Sacoche', 'Sacoche pratique', 20, 20, null, '2018-10-21', NOW()),
    (1, 8, 'Vends vélo jaune neuf', 'Bonjour, je vends ce vélo jaune en parfait état, très peu utilisé.', 80, 80, null, '2018-12-28', NOW()),
    (1, 2, 'Vends Fifa 19', 'Salut, je vends ce jeu nul qui est Fifa 19, que de la chance pour mes adversaires, je gagne pas un match.. PS: disque un peu tordu', 20, 20, null, '2018-10-31', NOW()),
    (1, 21, 'Vends balai', 'Cc, ayant acheté un aspirateur, je vends ce balai qui m''a rendu de loyaux services.', 5, 5, 'Public/pictures/1/252161561/pictureAd.jpeg', '2018-12-23', NOW()),
    (3, 1, 'Vends câble ethernet', 'Bonjour ou bonsoir, je vends des câbles ethernet de longeur variable, 1€ le mètre, 50 mètres disponibles en tout.', 50, 50, 'Public/pictures/3/252161561/pictureAd.jpg', '2018-11-13', NOW()),
    (3, 8, 'Vends vélo rouge neuf', 'Bonjour, je vends ce vélo rouge, jantes chromées, freins à disques, embrayage à diaphragme, pare-boue à l''arrière, pare-buffle à l''avant, gilet réfléchissant offert', 150, 150, 'Public/pictures/3/2656dz651d5c/pictureAd.jpg', '2018-12-28', NOW()),
    (4, 6, 'Vends ma collection d''albums de Jul', 'Salut, en grande difficulté financière, c''est à grand regret que je mets en vente ma collection complète d''albums de Jul dont un dédicacé par Benjamin Mendy', 200, 200, 'Public/pictures/4/464651/pictureAd.jpg', '2018-12-30', NOW());
