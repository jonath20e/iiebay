<?php 
/**
 * Génération automatique de fichiers pour aller plus vite
 * Exemple : php bin/console --view "nom du fichier vue" --controller "UnFichierController"
 * Puis, ajouter une route dans le fichier config
 */
define("ROUTE_VIEW",  "src/View/");
define("ROUTE_CONTROLLER",  "src/Controller/");

$options = getopt("", [
    "view:",
    "controller:"
]);

if (isset($options["view"]) && isset($options["controller"])) {
    $options["view"] .= ".php";

    print "Génération des fichiers en cours.." . PHP_EOL . $options["controller"] . PHP_EOL . $options["view"] . PHP_EOL;

    if (!is_dir(ROUTE_VIEW) || !is_dir(ROUTE_CONTROLLER)) {
        print "Il y a un problème de structure dans le projet";
        exit(1);
    }
    if (file_exists(ROUTE_VIEW . $options["view"]) || file_exists(ROUTE_CONTROLLER . $options["controller"] . "Controller.class.php")) {
        print "La génération a échoué car des fichiers de même nom existent";
        exit(1);
    }
    $bool_controller = file_put_contents(ROUTE_CONTROLLER . $options["controller"] . "Controller.class.php", '<?php

namespace App\Controller;

use \App\App;

class ' . $options["controller"] . "Controller" . ' extends Controller {
    private $DB;
    private $CONFIG;
    private $PRARAM;
    
    public function __construct(\PDO & $DB, ?array & $CONFIG, ?array & $PARAM) {
        $this->DB = $DB;
        $this->CONFIG = $CONFIG;
        $this->PARAM = $PARAM;
        parent::__construct("' . $options["view"] .'");
    }

    public function render() {
            
        require_once("View/" . $this->template);
    }
}

App::render(__NAMESPACE__, __FILE__, $DB, $config, $PARAM);

?>
    ');
    if ($bool_controller) {
            $bool_view = file_put_contents(ROUTE_VIEW . $options["view"], '<!DOCTYPE html>
<html lang="fr">
  <head>
		<meta charset="utf-8">
		<?php require_once("require_link.php"); ?>
    <title> Creez dès maintenant votre enchère ! </title>
  </head>
<body>
	<?php require_once("nav.php"); ?>
		<div class="container">
			
		</div>
	</body>
</html>
    ');
        if ($bool_view) {
            print "Génération terminée" . PHP_EOL;
            system("composer dumpauto -o");
            exit(1);
        }else {
            unlink(ROUTE_CONTROLLER . $options["controller"]);
            print "La génération du view a échoué"; 
            exit(1);
        }
    }else {
        print "La génération du controller a échoué";
        exit(1);
    }
    exit(1);
}
print "Paramètres incorrectes";


?>