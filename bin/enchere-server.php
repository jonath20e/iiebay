<?php
use Ratchet\Server\IoServer;
use Ratchet\Http\HttpServer;
use Ratchet\WebSocket\WsServer;

use App\Module\Request\EnchereTransation;

    require dirname(__DIR__) . '/vendor/autoload.php';

    print "Lancement du serveur...." . PHP_EOL;
    
    $serverApp = new WsServer(
        new EnchereTransation()
    );
    $serverApp->enableKeepAlive(React\EventLoop\Factory::create(), 2);
    $server = IoServer::factory(
        new HttpServer(
            $serverApp
        ),
        8080
    );


    $server->run();
