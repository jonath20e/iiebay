# Rapport d'itération  
*Ce rapport est à fournir pour chaque équipe au moment du changement d'itération.*

## Composition de l'équipe 
*Remplir ce tableau avec la composition de l'équipe et les rôles.*

|  &nbsp;                 | Itération précédente     |
| -------------           |-------------             |
| **Product Owner**       | Solène CARLO             |
| **Scrum Mast6er**       | Macil GUIDDIR            |

## Bilan de l'itération précédente  
### Évènements 
*Quels sont les évènements qui ont marqué l'itération précédente? Répertoriez ici les évènements qui ont eu un impact sur ce qui était prévu à l'itération précédente.*
> Les événements ayant eu un impact sur l'it&ration précédente est : 
> *la semaine de soutenances de stage*
> *l'arrivée soudaine de plusieurs projets à la fois*
> *les démarches pour les séjours à l'étranger.*


### Taux de complétion de l'itération  
*Quel est le nombre d'éléments terminés par rapport au nombre total d'éléments prévu pour l'itération?*
> 2/3 (67%)

### Liste des User Stories terminées
*Quelles sont les User Stories qui ont été validées par le PO à la fin de l'itération ?*
> Recherche d'une annonce validée et base de données aussi. Certains éléments manquent dans la mise en ligne d'une annonce.
## Rétrospective de l'itération précédente
  
### Bilans des retours et des précédentes actions 
*Quels sont les retours faits par l'équipe pendant la rétrospective? Quelles sont les actions qui ont apporté quelque chose ou non?*
> Points positifs :
*Globalement, tout le monde s'est cantonné dans les tâches qui lui ont été attribuées*
*On a fait des daily-meetings fréquents où tout le monde était là*
*Stand up meetings : respect de la forme "ce que j'ai fait, ce que je vais faire et pb rencontrés"*
*Evaluation des tâches : se passe très bien. Les storypoints sont données sous forme de Fibonacci (sauf 21 qui devient 20)*
*Les US sont sorties*
*Entraide PHP !*
*Du bon boulot pour une première version : c'est ce qu'on voulait*

> Points négatifs :
*Lors de la démo, utiliser la forme "la US était de faire ça. On a fait ça"*
*On ne respectait pas les 12 minutes lors des stand up meetings. On dépassait jusqu'à 15 minutes car on avait trop de choses à dire*
*Backlog un peu lourd. Se limiter à moins d'US*
*Problème rencontré : manque de temps car beaucoup de projets dans les autres matières d'un coup*
*Préparer présentation tôt pour que mercredi = bilan du product owner*
*Répartition des tâches : y revenir (avant : manque de temps)*
*On s'est sous-estimé*
*Problèmes logiciels pgAdmin*
*Quand on passe le travail à john pour le php, il faut que ce soit nickel*




### Actions prises pour la prochaine itération
*Quelles sont les actions décidées par l'équipe pour la prochaine itération ?*
 > Actions décidées :
*Backlog moins lourd*
*Mettre l'application chronomètre lors des stand up !*
*Distribuer les tâches jeudi matin pas l'après-midi*
*S'y prendre Plus tôt*


### Axes d'améliorations 
*Quels sont les axes d'améliorations pour les personnes qui ont tenu les rôles de PO, SM et Dev sur la précédente itération?*
> Points d'amélioration :
*Backlog moins lourd*
*Mettre l'application chronomètre lors des stand up !*
*Distribuer les tâches jeudi matin pas l'après-midi*
*S'y prendre Plus tôt*



## Prévisions de l'itération suivante  
### Évènements prévus  
*Quels sont les évènements qui vont peut être impacter l'itération? Répertoriez ici les évènements que vous anticipez comme ayant un impact potentiel pour l'itération (absences, changement de cap, difficultés, etc.).*
> Les événéments à venir seront :
*Les vacances à venir*

### Titre des User Stories reportées  
*Lister ici les éléments des itérations précédentes qui ont été reportés à l'itération suivante. Ces éléments ont dû être revus et corrigés par le PO.*
> Les éléments reportés à l'itération suivante :
*Mise en ligne d'une annonce*
*Complétion de la base de données*

### Titre des nouvelles User Stories  
*Lister ici les nouveaux éléments pour l'itération suivante. Ces éléments ont dû être revus et corrigés par le PO.*
> mettre en ligne une annonce
_ enchère
- recherche d'un produit
- Menu
- Incription
- Connexion / Deconnexion
- Profil/Modification du profil
- Design

## Confiance 
### Taux de confiance de l'équipe dans l'itération  
*Remplir le tableau sachant que :D est une confiance totale dans le fait de livrer les éléments de l'itération. Mettre le nombre de votes dans chacune des cases. Expliquer en cas de besoin.*

|          	| :( 	| :&#124; 	| :) 	| :D 	|
|:--------:	|:----:	|:----:	    |:----:	|:----:	|
| Equipe 6	|  *0* 	|  *0*	    |  5	|  1 	|

### Taux de confiance de l'équipe pour la réalisation du projet 
*Remplir le tableau sachant que :D est une confiance totale dans le fait de réaliser le projet. Mettre le nombre de votes dans chacune des cases. Expliquer en cas de besoin.*

|          	| :( 	| :&#124; 	| :) 	| :D 	|
|:--------:	|:----:	|:----:	    |:----:	|:----:	|
| Equipe 7 	|  *0* 	|  *0* 	    |  2 	|  4 	|


## Composition de l'équipe 
*Remplir ce tableau avec la composition de l'équipe et les rôles.*

|  &nbsp;                 | Itération précédente     |
| -------------           |-------------             |
| **Product Owner**       | Solène CARLO             |
| **Scrum Mast6er**       | Christian MORELLO        |

## Bilan de l'itération précédente  
### Évènements 
*Quels sont les évènements qui ont marqué l'itération précédente? Répertoriez ici les évènements qui ont eu un impact sur ce qui était prévu à l'itération précédente.*
> Les événements ayant eu un impact sur l'it&ration précédente est : 
> *les vacances de la Toussaint*


### Taux de complétion de l'itération  
*Quel est le nombre d'éléments terminés par rapport au nombre total d'éléments prévu pour l'itération?*
> 4/5 (80%)

### Liste des User Stories terminées
*Quelles sont les User Stories qui ont été validées par le PO à la fin de l'itération ?*
> En tant qu'utilisateur, j'ai besoin de pouvoir me connecter au site avec mon identifiant et mon mot de passe
> L'utilisateur souhaite pouvoir naviguer facilement entre les différentes parties du site
> En tant qu'utilisateur, je veux pouvoir m'inscrire sur IIE Bay afin de pouvoir acheter ou vendre un produit
> En tant qu'utilisateur de IIEbay, je dois pouvoir me connecter à un serveur qui effectue des requêtes avec une base de données

## Rétrospective de l'itération précédente
  
### Bilans des retours et des précédentes actions 
*Quels sont les retours faits par l'équipe pendant la rétrospective? Quelles sont les actions qui ont apporté quelque chose ou non?*
> Points positifs :
*Correction des fautes d'orthographe et d'interface*
*Tout le monde a (presque) réalisé ses tâches*
*Stand up meetings : respect de la forme "ce que j'ai fait, ce que je vais faire et pb rencontrés"*
*Evaluation des tâches : se passe très bien. Les storypoints sont données sous forme de Fibonacci (sauf 21 qui devient 20)*
*Bonne estimation de complexité, assez d'US dans le planning. Meilleur remplissage du backlog*
*Respect de l'US dans la démo : "en tant que ..." et on montre cette fonctionnalité.*


> Points négatifs :
*US pas assez détaillée (remarque présentation)*
*Mauvais pic d'activité la veille (combination de tous les travaux)*
*Une US pas totalement finie*





### Actions prises pour la prochaine itération
*Quelles sont les actions décidées par l'équipe pour la prochaine itération ?*
 > Actions décidées :
*Mieux détailler les US (rajouter le "pour")*
*Mieux gérer le temps pour rassembler les travaux de tous*
*Mieux gérer la répartition des tâches selon les complexités*



### Axes d'améliorations 
*Quels sont les axes d'améliorations pour les personnes qui ont tenu les rôles de PO, SM et Dev sur la précédente itération?*
> Points d'amélioration :
*Mieux détailler les US (rajouter le "pour")*
*Mieux gérer le temps pour rassembler les travaux de tous*
*Mieux gérer la répartition des tâches selon les complexités*



## Prévisions de l'itération suivante  
### Évènements prévus  
*Quels sont les évènements qui vont peut être impacter l'itération? Répertoriez ici les évènements que vous anticipez comme ayant un impact potentiel pour l'itération (absences, changement de cap, difficultés, etc.).*
> Les événéments à venir seront :
*Beaucoup de projets à venir*
*Quelques partiels à venir*

### Titre des User Stories reportées  
*Lister ici les éléments des itérations précédentes qui ont été reportés à l'itération suivante. Ces éléments ont dû être revus et corrigés par le PO.*
> Les éléments reportés à l'itération suivante :
*Design*

### Titre des nouvelles User Stories  
*Lister ici les nouveaux éléments pour l'itération suivante. Ces éléments ont dû être revus et corrigés par le PO.*
- En tant qu'utilisateur je veux pouvoir avoir une interface conviviale afin d'avoir une expérience agréable sur le site
- En tant qu'acheteur, je veux pouvoir enchérir sur un produit avec une certaine incrémentation pour pouvoir avoir une chance d'acquérir le produit 
- En tant qu'utilisateur je veux pouvoir modifier mon adresse mail afin de pouvoir faire face à un éventuel changement d'adresse
- En tant qu'utilisateur je veux pouvoir modifier mon prénom et mon nom de famille en cas d'erreur initiale
- En tant qu'acheteur, je veux pouvoir contacter mon vendeur (avoir accès à son email) afin de pouvoir échanger avec lui vis à vis du produit.
- En tant qu'utilisateur je veux pouvoir modifier mon pseudo afin de pouvoir faire face à une éventuelle volonté de changer mon pseudo 
- En tant qu'utilisateur je veux pouvoir modifier mon mot de passe afin de pouvoir faire face à une éventuelle volonté de changer mon mot de passe
- En tant qu'utilisateur, je veux pouvoir trier les produits de ma recherche sous certains critères afin de pouvoir faire une recherche plus efficace
- En tant que vendeur, je veux pouvoir noter mon acheteur afin que les autres utilisateurs puissent avoir un premier avis sur lui

## Confiance 
### Taux de confiance de l'équipe dans l'itération  
*Remplir le tableau sachant que :D est une confiance totale dans le fait de livrer les éléments de l'itération. Mettre le nombre de votes dans chacune des cases. Expliquer en cas de besoin.*

|          	| :( 	| :&#124; 	| :) 	| :D 	|
|:--------:	|:----:	|:----:	    |:----:	|:----:	|
| Equipe 6	|  *0* 	|  *0*	    |  *4*	|  *2*	|

### Taux de confiance de l'équipe pour la réalisation du projet 
*Remplir le tableau sachant que :D est une confiance totale dans le fait de réaliser le projet. Mettre le nombre de votes dans chacune des cases. Expliquer en cas de besoin.*

|          	| :( 	| :&#124; 	| :) 	| :D 	|
|:--------:	|:----:	|:----:	    |:----:	|:----:	|
| Equipe 7 	|  *0* 	|  *0* 	    |  *3*	|  *3*	|




## Composition de l'équipe 
*Remplir ce tableau avec la composition de l'équipe et les rôles.*

|  &nbsp;                 | Itération précédente     |
| -------------           |-------------             |
| **Product Owner**       | Solène CARLO             |
| **Scrum Mast6er**       | Lucas  HOLZMANN          |

## Bilan de l'itération précédente  
### Évènements 
*Quels sont les évènements qui ont marqué l'itération précédente? Répertoriez ici les évènements qui ont eu un impact sur ce qui était prévu à l'itération précédente.*
> Les événements ayant eu un impact sur l'it&ration précédente est : 
> *Des partiels et projets à rendre*


### Taux de complétion de l'itération  
*Quel est le nombre d'éléments terminés par rapport au nombre total d'éléments prévu pour l'itération?*
> 7/10 (70%)

### Liste des User Stories terminées
*Quelles sont les User Stories qui ont été validées par le PO à la fin de l'itération ?*
- En tant qu'utilisateur je veux pouvoir avoir une interface conviviale afin d'avoir une expérience agréable sur le site
- En tant qu'acheteur, je veux pouvoir enchérir sur un produit avec une certaine incrémentation pour pouvoir avoir une chance d'acquérir le produit 
- En tant qu'utilisateur je veux pouvoir modifier mon adresse mail afin de pouvoir faire face à un éventuel changement d'adresse
- En tant qu'utilisateur je veux pouvoir modifier mon prénom et mon nom de famille en cas d'erreur initiale
- En tant qu'utilisateur je veux pouvoir modifier mon pseudo afin de pouvoir faire face à une éventuelle volonté de changer mon pseudo 
- En tant qu'utilisateur je veux pouvoir modifier mon mot de passe afin de pouvoir faire face à une éventuelle volonté de changer mon mot de passe
- En tant qu'utilisateur, je veux pouvoir trier les produits de ma recherche sous certains critères afin de pouvoir faire une recherche plus efficace

## Rétrospective de l'itération précédente
  
### Bilans des retours et des précédentes actions 
*Quels sont les retours faits par l'équipe pendant la rétrospective? Quelles sont les actions qui ont apporté quelque chose ou non?*
> Points positifs :
*US plus détaillées que la dernière fois ce qui nous a permis de mieux nous concentrer sur l'essentiel*
*Bonne communication et bon rythme de meetings/standups*


> Points négatifs :
*Mauvaise gestion du temps: upload des travaux tardif*
*Dépassement de temps dans certains standups*





### Actions prises pour la prochaine itération
*Quelles sont les actions décidées par l'équipe pour la prochaine itération ?*
 > Actions décidées :
*Mieux gérer le temps passé dans les standups (ramener un chrono)*
*Mieux gérer le temps pour rassembler les travaux de tous*



### Axes d'améliorations 
*Quels sont les axes d'améliorations pour les personnes qui ont tenu les rôles de PO, SM et Dev sur la précédente itération?*
> Points d'amélioration :
*Mieux gérer le temps passé dans les standups (ramener un chrono)*
*Mieux gérer le temps pour rassembler les travaux de tous*



## Prévisions de l'itération suivante  
### Évènements prévus  
*Quels sont les évènements qui vont peut être impacter l'itération? Répertoriez ici les évènements que vous anticipez comme ayant un impact potentiel pour l'itération (absences, changement de cap, difficultés, etc.).*
> Les événéments à venir seront :
*Beaucoup de projets à venir*
*Quelques partiels à venir*
*Le forum des entreprises*


### Titre des User Stories reportées  
*Lister ici les éléments des itérations précédentes qui ont été reportés à l'itération suivante. Ces éléments ont dû être revus et corrigés par le PO.*
> Les éléments reportés à l'itération suivante :
*Système de notation*
*Système de contact acheteur/vendeur*

### Titre des nouvelles User Stories  
*Lister ici les nouveaux éléments pour l'itération suivante. Ces éléments ont dû être revus et corrigés par le PO.*
- En tant qu'acheteur, je veux pouvoir noter mon vendeur afin que les autres utilisateurs puissent avoir un premier avis sur lui.
- En tant que vendeur, je veux pouvoir noter mon acheteur afin que les autres utilisateurs puissent avoir un premier avis sur lui.
- En tant qu'administrateur je veux pouvoir gérer les comptes des utilisateurs afin de pouvoir contrôler les différentes comptes de mon site.
- En tant qu'acheteur, je veux pouvoir contacter mon vendeur (avoir accès à son email) afin de pouvoir échanger avec lui vis à vis du produit.
- En tant que vendeur, je souhaite pouvoir clôturer une enchère avant son terme pour vendre mon produit.
- En tant que vendeur je veux pouvoir supprimer une enchère faite par un acheteur si j'estime qu'elle est biaisée.
- En tant qu'acheteur je veux pouvoir avoir accès rapidement à mes enchères en cours.
- En tant que vendeur je veux pouvoir avoir accès rapidement à mes ventes en cours.


## Confiance 
### Taux de confiance de l'équipe dans l'itération  
*Remplir le tableau sachant que :D est une confiance totale dans le fait de livrer les éléments de l'itération. Mettre le nombre de votes dans chacune des cases. Expliquer en cas de besoin.*

|          	| :( 	| :&#124; 	| :) 	| :D 	|
|:--------:	|:----:	|:----:	    |:----:	|:----:	|
| Equipe 6	|  *0* 	|  *0*	    |  *6*	|  *0*	|

### Taux de confiance de l'équipe pour la réalisation du projet 
*Remplir le tableau sachant que :D est une confiance totale dans le fait de réaliser le projet. Mettre le nombre de votes dans chacune des cases. Expliquer en cas de besoin.*

|          	| :( 	| :&#124; 	| :) 	| :D 	|
|:--------:	|:----:	|:----:	    |:----:	|:----:	|
| Equipe 7 	|  *0* 	|  *0* 	    |  *4*	|  *2*	|



## Composition de l'équipe 
*Remplir ce tableau avec la composition de l'équipe et les rôles.*

|  &nbsp;                 | Itération précédente     |
| -------------           |-------------             |
| **Product Owner**       | Solène CARLO             |
| **Scrum Mast6er**       | François  Bourel         |

## Bilan de l'itération précédente  
### Évènements 
*Quels sont les évènements qui ont marqué l'itération précédente? Répertoriez ici les évènements qui ont eu un impact sur ce qui était prévu à l'itération précédente.*
> Les événements ayant eu un impact sur l'it&ration précédente est : 
> *Des partiels et projets à rendre encore*
> *Campagne BDE*
> *Forum entreprise*


### Taux de complétion de l'itération  
*Quel est le nombre d'éléments terminés par rapport au nombre total d'éléments prévu pour l'itération?*
> 6/6 (100%)

### Liste des User Stories terminées
*Quelles sont les User Stories qui ont été validées par le PO à la fin de l'itération ?*
- En tant qu'administrateur je veux pouvoir gérer les comptes des utilisateurs afin de pouvoir contrôler les différentes comptes de mon site
- En tant qu'acheteur, je veux pouvoir contacter mon vendeur (avoir accès à son email) afin de pouvoir échanger avec lui vis à vis du produit.
- En tant que vendeur, je souhaite pouvoir clôturer une enchère avant son terme pour vendre mon produit.
- En tant que vendeur je veux pouvoir supprimer une enchère faite par un acheteur si j'estime qu'elle est biaisée.
- En tant que vendeur je veux pouvoir avoir accès rapidement à mes ventes en cours.
- En tant qu'acheteur je veux pouvoir avoir accès rapidement à mes enchères en cours.

## Rétrospective de l'itération précédente
  
### Bilans des retours et des précédentes actions 
*Quels sont les retours faits par l'équipe pendant la rétrospective? Quelles sont les actions qui ont apporté quelque chose ou non?*
> Points positifs :
*Bonne communication et bon rythme de meetings/standups (tout les 3 jours)*
*Bonne réalisation du sprint, bonne anticipation du sprint ce qui nous a permis de travailler des base de la sécurité*
*Repport des US sur la notation à cause de la campagne BDE qui n'avais pas été envisagé lors du sprint planning*



> Points négatifs :
*Non utilisation du git a cause de probleme internet pour certain*


### Titre des User Stories reportées  
*Lister ici les éléments des itérations précédentes qui ont été reportés à l'itération suivante. Ces éléments ont dû être revus et corrigés par le PO.*
> Les éléments reportés à l'itération suivante :
- En tant qu'acheteur, je veux pouvoir noter mon vendeur afin que les autres utilisateurs puissent avoir un premier avis sur lui.
- En tant que vendeur, je veux pouvoir noter mon acheteur afin que les autres utilisateurs puissent avoir un premier avis sur lui.




 ## Retrospective sur le projet entier 

### Satisfaction de l'équipe sur l'ensemble du projet 
*Remplir le tableau sachant que :D est une confiance totale dans le fait de réaliser le projet. Mettre le nombre de votes dans chacune des cases. Expliquer en cas de besoin.*

|          	| :( 	| :&#124; 	| :) 	| :D 	|
|:--------:	|:----:	|:----:	    |:----:	|:----:	|
| Equipe 7 	|  *0* 	|  *0* 	    |  *4*	|  *2*	|
 
 ### Commentaires
 Au début du projet, nous avons du faire face à des problèmes du à notre manque d'expérience vis à vis de la méthode agile. Puis nous nous sommes améliorés au fur et à mesure des sprints. Grâce aux retrospectives nous avons pu noter des axes d'amélioration à la fin de chaque sprint. Ainsi lors du sprint suivant nous avons tenu compte de ces remarques. Cela nous a permis d'être plus efficace et d'être plus agile. 
 Nous sommes contents de ce que nous avons réalisé. Cela nous a permis d'apprendre à travailler agilement. Nous avons trouvé la méthode très efficice et nous la remettrons en place lors de nos porchains projets. Nous nous sentons désormais à l'aise avec cette méthode et nous nous sentons capable d'intégrer une équipe agile malgré que nous avons, certes, encore de l'expérience à acquérir. Nous avons tout de même conscience que nous avions un avantage considérable par rapport à certaines autres équipe travaillant ensemble : nous avons déjà travailler ensemble et nous nous connaissions avant et donc on connaissait les points forts de chacun. Nous avons été une bonne équipe: à l'écoute les uns des autres et complémentaires sur les tâches. 

 Nous avons juste un regret : nous aurions aimé avoir le temps de pouvoir ajouter des fonctionnalités à notre site web. Nous avions encore des user story que nous aurions souhaité mettre en place. Si nous avions réussià être aussi efficace qu'aux deux derniers sprints nous aurions peut-être réussi à tout faire. 

 De façon g&nérale, le SCRUM a su nous placer, tous les 6, dans un cadre rigoureux. Ce cadre, dans lequel nous devions rendre des comptes, nous a forcés à être sincère envers les autres, et donc à mieux anticiper les imprévus (autres projets, partiels, etc.). La partie "problèmes rencontrés" de nos stand-up meetings (tous les 3 jours environ) nous a montré que nous pouvions être sincère concernant les raisons pour lesquelles nous avons pris du retard, et que ces raisons sont JUSTIFIABLES !

 Enfin, contrairement à la plupart des cas en entreprise, nous nous sommes choisis tous les 6, ce qui est une chance. Ainsi, la communication était beaucoup plus facile. Et même si nous avions des problèmes techniques sur le Git par exemple (problèmes pour pousser notre travail), les membres de l'équipe se sont entraidés à l'aide de "séances Team Viewer". Ce qui ne sera pas forcément le cas après le diplôme, quand nos collègues seront rentrés chez eux...


